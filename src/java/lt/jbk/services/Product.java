/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.services;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import lt.jbk.entities.Categories;
import lt.jbk.entities.Products;
import lt.jbk.resources.EMF;
import lt.jbk.resources.Status;

/**
 *
 * @author Justi
 */
@Path("products")
public class Product {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Products> getProducts(@QueryParam("cid") Integer cid, @QueryParam("sort") String sort, @QueryParam("col") String col) {
        EntityManager em = EMF.getEntityManager();
        List<Products> resultList;
        
        if (cid != null) {
            Query q = em.createQuery("SELECT p FROM Products p WHERE p.categoryId = :cid");
            q.setParameter("cid", em.find(Categories.class, cid));
            resultList = q.getResultList();
        } else {
            Query q = em.createNamedQuery("Products.findAll");
            resultList = q.getResultList();
        }
        
        EMF.returnEntityManager(em);
        
        if ("esc".equals(sort)) {
            if ("name".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getName().compareTo(y.getName())).collect(Collectors.toList());
            }
            
            if ("desc".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getDescription().compareTo(y.getDescription())).collect(Collectors.toList());
            }
            
            if ("quantity".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> Long.compare(x.getQuantity(), y.getQuantity())).collect(Collectors.toList());
            }
            
            if ("price".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getPrice().compareTo(y.getPrice())).collect(Collectors.toList());
            }
            
            if ("category".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getCategoryId().getName().compareTo(y.getCategoryId().getName())).collect(Collectors.toList());
            }
        }
        
        if ("desc".equals(sort)) {
            if ("name".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getName().compareTo(x.getName())).collect(Collectors.toList());
            }
            
            if ("desc".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getDescription().compareTo(x.getDescription())).collect(Collectors.toList());
            }
            
            if ("quantity".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> Long.compare(y.getQuantity(), x.getQuantity())).collect(Collectors.toList());
            }
            
            if ("price".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getPrice().compareTo(x.getPrice())).collect(Collectors.toList());
            }
            
            if ("category".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getCategoryId().getName().compareTo(x.getCategoryId().getName())).collect(Collectors.toList());
            }
        }
        
        return resultList;
    }
    
    @GET
    @Path("cats")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Products> getProductsByCategories(@QueryParam("cats") String cats) {
        String[] categoriesId = cats.split(":");
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p FROM Products p WHERE p.categoryId = ").append(categoriesId[0]);
        
        for (int i = 1; i < categoriesId.length; ++i) {
            sb.append("OR p.categoryId = ").append(categoriesId[i]);
        }
        
        EntityManager em = EMF.getEntityManager();
        Query q = em.createQuery(sb.toString());
        List<Products> resultList = q.getResultList();
        EMF.returnEntityManager(em);
        
        return resultList;
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status addProduct(@QueryParam("cid") Integer cid, Products product) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        
        Categories foundCategory = em.find(Categories.class, cid);
        
        if (foundCategory == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Category don't exist!");
            
            return status;
        }
        
        Query q = em.createQuery("SELECT p FROM Products p WHERE p.categoryId = :cid AND p.name = :name");
        q.setParameter("cid", foundCategory);
        q.setParameter("name", product.getName());
        List resultList = q.getResultList();
        
        if (!resultList.isEmpty()) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Product in this category already exists!");
            
            return status;
        }
        
        EntityTransaction tx = EMF.getTransaction(em);
        product.setCategoryId(foundCategory);
        em.persist(product);
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Products getProduct(@PathParam("id") Integer id) {
        EntityManager em = EMF.getEntityManager();
        Products foundProduct = em.find(Products.class, id);
        EMF.returnEntityManager(em);
        
        return foundProduct;
    }
    
    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status editProduct(@PathParam("id") Integer id, Products product) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        Products foundProduct = em.find(Products.class, id);
        
        if (foundProduct == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Product not found!");
            
            return status;
        }
        
        Query q = em.createQuery("SELECT p FROM Products p WHERE p.categoryId = :cid AND p.name = :name");
        q.setParameter("cid", foundProduct.getCategoryId());
        q.setParameter("name", product.getName());
        List<Products> resultList = q.getResultList();
        
        if (!resultList.isEmpty() && !resultList.get(0).getName().equals(product.getName())) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Product in this category already exists!");
            
            return status;
        }
        
        EntityTransaction tx = EMF.getTransaction(em);
        foundProduct.setName(product.getName());
        foundProduct.setDescription(product.getDescription());
        foundProduct.setPrice(product.getPrice());
        foundProduct.setImage(product.getImage());
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Status deleteProduct(@PathParam("id") Integer id) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        Products foundProduct = em.find(Products.class, id);
        
        if (foundProduct == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            
            return status;
        }
        
        EntityTransaction tx = EMF.getTransaction(em);
        em.remove(foundProduct);
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
}