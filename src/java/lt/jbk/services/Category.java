/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.services;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import lt.jbk.entities.Categories;
import lt.jbk.resources.EMF;
import lt.jbk.resources.Status;

/**
 *
 * @author Justi
 */
@Path("categories")
public class Category {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Categories> getCategories(@QueryParam("sort") String sort, @QueryParam("order") String order) {
        EntityManager em = EMF.getEntityManager();
        Query q = em.createNamedQuery("Categories.findAll");
        List<Categories> resultList = q.getResultList();
        
        resultList.forEach((category) -> {
            Query q1 = em.createQuery("SELECT p FROM Products p WHERE p.categoryId = :cid");
            q1.setParameter("cid", category);
            List resultList1 = q1.getResultList();
            category.setCount(resultList1.size());
        });
        
        if ("esc".equals(sort)) {
            if ("name".equals(order)) {
                resultList = resultList.stream().sorted((x, y) -> x.getName().compareTo(y.getName())).collect(Collectors.toList());
            }
            
            if ("count".equals(order)) {
                resultList = resultList.stream().sorted((x, y) -> x.getCount().compareTo(y.getCount())).collect(Collectors.toList());
            }
        }
        
        if ("desc".equals(sort)) {
            if ("name".equals(order)) {
                resultList = resultList.stream().sorted((x, y) -> y.getName().compareTo(x.getName())).collect(Collectors.toList());
            }
            
            if ("count".equals(order)) {
                resultList = resultList.stream().sorted((x, y) -> y.getCount().compareTo(x.getCount())).collect(Collectors.toList());
            }
        }
        
        EMF.returnEntityManager(em);
        
        return resultList;
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status addCategory(Categories category) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        Query q = em.createNamedQuery("Categories.findByName");
        q.setParameter("name", category.getName());
        List resultList = q.getResultList();
        
        if (!resultList.isEmpty()) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Category name already exists!");
            
            return status;
        }
        
        EntityTransaction tx = EMF.getTransaction(em);
        em.persist(category);
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Categories getCategory(@PathParam("id") Integer id) {
        EntityManager em = EMF.getEntityManager();
        Categories foundCategory = em.find(Categories.class, id);
        EMF.returnEntityManager(em);
        
        return foundCategory;
    }
    
    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status editCategory(@PathParam("id") Integer id, Categories category) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        Categories foundCategory = em.find(Categories.class, id);
        
        if (foundCategory == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Category not found!");
            
            return status;
        }
        
        Query q = em.createNamedQuery("Categories.findByName");
        q.setParameter("name", category.getName());
        List resultList = q.getResultList();
        
        if (!resultList.isEmpty()) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Category name already exists!");
            
            return status;
        }
        
        EntityTransaction tx = EMF.getTransaction(em);
        foundCategory.setName(category.getName());
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Status deletecategory(@PathParam("id") Integer id) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        Categories foundCategory = em.find(Categories.class, id);
        
        if (foundCategory == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            
            return status;
        }
        
        EntityTransaction tx = EMF.getTransaction(em);
        em.remove(foundCategory);
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
}