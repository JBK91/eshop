/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import lt.jbk.entities.CartData;
import lt.jbk.entities.Carts;
import lt.jbk.entities.UserAddresses;
import lt.jbk.entities.UserInfo;
import lt.jbk.entities.UserPaymentInfo;
import lt.jbk.entities.Users;
import lt.jbk.resources.EMF;
import lt.jbk.resources.Status;

/**
 *
 * @author Justi
 */
@Path("profile")
public class Profile {
    @Context HttpServletRequest request;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Users getUser() {
        Users user = null;
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            String username = (String) session.getAttribute("logged");
            
            if (username != null) {
                EntityManager em = EMF.getEntityManager();
                Query q = em.createNamedQuery("Users.findByUsername");
                q.setParameter("username", username);
                List<Users> resultList = q.getResultList();
                EMF.returnEntityManager(em);
                
                if (!resultList.isEmpty()) {
                    user = resultList.get(0);
                }
            }
        }
        
        return user;
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status updateEmail(Users user) throws Exception {
        Status status = new Status();
        status.setStatus("failed");
        status.setMessage("User not found!");
        
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            String username = (String) session.getAttribute("logged");
            
            if (username != null) {
                EntityManager em = EMF.getEntityManager();
                Query q = em.createNamedQuery("Users.findByUsername");
                q.setParameter("username", username);
                List<Users> resultList = q.getResultList();
                
                if (!resultList.isEmpty()) {
                    EntityTransaction tx = EMF.getTransaction(em);
                    resultList.get(0).setEmail(user.getEmail());
                    EMF.commitTransaction(tx);
                    
                    status.setStatus("success");
                }
                
                EMF.returnEntityManager(em);
            }
        }
        
        return status;
    }
    
    @GET
    @Path("info")
    @Produces(MediaType.APPLICATION_JSON)
    public UserInfo getUserInfo() {
        UserInfo userInfo = null;
        
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            String username = (String) session.getAttribute("logged");
            
            if (username != null) {
                EntityManager em = EMF.getEntityManager();
                Query q = em.createNamedQuery("Users.findByUsername");
                q.setParameter("username", username);
                List<Users> resultList = q.getResultList();
                
                if (!resultList.isEmpty()) {
                    Users user = resultList.get(0);
                    
                    Query q1 = em.createQuery("SELECT ui FROM UserInfo ui WHERE ui.userId = :userId");
                    q1.setParameter("userId", user);
                    List<UserInfo> resultList1 = q1.getResultList();
                    
                    if (!resultList1.isEmpty()) {
                        userInfo = resultList1.get(0);
                    }
                }
                
                EMF.returnEntityManager(em);
            }
        }
        
        return userInfo;
    }
    
    @POST
    @Path("info")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status updateInfo(UserInfo userInfo) throws Exception {
        Status status = new Status();
        status.setStatus("failed");
        status.setMessage("User not found!");
        
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            String username = (String) session.getAttribute("logged");
            
            if (username != null) {
                EntityManager em = EMF.getEntityManager();
                Query q = em.createNamedQuery("Users.findByUsername");
                q.setParameter("username", username);
                List<Users> resultList = q.getResultList();
                
                if (!resultList.isEmpty()) {
                    Users user = resultList.get(0);
                    
                    Query q1 = em.createQuery("SELECT ui FROM UserInfo ui WHERE ui.userId = :userId");
                    q1.setParameter("userId", user);
                    List<UserInfo> resultList1 = q1.getResultList();
                    
                    EntityTransaction tx = EMF.getTransaction(em);
                    
                    if (resultList1.isEmpty()) {
                        userInfo.setUserId(user);
                        em.persist(userInfo);
                    } else {
                        resultList1.get(0).setFirstName(userInfo.getFirstName());
                        resultList1.get(0).setLastName(userInfo.getLastName());
                        resultList1.get(0).setPhoneNumber(userInfo.getPhoneNumber());
                    }
                    
                    status.setStatus("success");
                    
                    EMF.commitTransaction(tx);
                    EMF.returnEntityManager(em);
                }
            }
        }
        
        return status;
    }
    
    @GET
    @Path("address")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserAddresses> getAddresses() {
        List<UserAddresses> userAddresses = null;
        
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            String username = (String) session.getAttribute("logged");
            
            if (username != null) {
                EntityManager em = EMF.getEntityManager();
                Query q = em.createNamedQuery("Users.findByUsername");
                q.setParameter("username", username);
                List<Users> resultList = q.getResultList();
                
                if (!resultList.isEmpty()) {
                    Query q1 = em.createQuery("SELECT ua FROM UserAddresses ua WHERE ua.userId = :userId");
                    q1.setParameter("userId", resultList.get(0));
                    userAddresses = q1.getResultList();
                }
                
                EMF.returnEntityManager(em);
            }
        }
        
        return userAddresses;
    }
    
    @GET
    @Path("address/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserAddresses getAddress(@PathParam("id") Integer id) {
        EntityManager em = EMF.getEntityManager();
        UserAddresses foundAddress = em.find(UserAddresses.class, id);
        EMF.returnEntityManager(em);
        
        return foundAddress;
    }
    
    @POST
    @Path("address")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status addAddress(UserAddresses address) throws Exception {
        Status status = new Status();
        status.setStatus("failed");
        status.setMessage("User not found!");
        
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            String username = (String) session.getAttribute("logged");
            
            if (username != null) {
                EntityManager em = EMF.getEntityManager();
                Query q = em.createNamedQuery("Users.findByUsername");
                q.setParameter("username", username);
                List<Users> resultList = q.getResultList();
                
                if (!resultList.isEmpty()) {
                    EntityTransaction tx = EMF.getTransaction(em);
                    address.setUserId(resultList.get(0));
                    em.persist(address);
                    EMF.commitTransaction(tx);
                    
                    status.setStatus("success");
                }
                
                EMF.returnEntityManager(em);
            }
        }
        
        return status;
    }
    
    @PUT
    @Path("address/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status editAddress(@PathParam("id") Integer id, UserAddresses address) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        UserAddresses foundAddress = em.find(UserAddresses.class, id);
        
        if (foundAddress == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Address not found!");
            
            return status;
        }
        
        EntityTransaction tx = EMF.getTransaction(em);
        foundAddress.setAddress(address.getAddress());
        foundAddress.setCity(address.getCity());
        foundAddress.setCountry(address.getCountry());
        foundAddress.setPostCode(address.getPostCode());
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @DELETE
    @Path("address/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Status deleteAddress(@PathParam("id") Integer id) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        UserAddresses foundAddress = em.find(UserAddresses.class, id);
        
        if (foundAddress == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Product not found!");
            
            return status;
        }
        
        EntityTransaction tx = EMF.getTransaction(em);
        em.remove(foundAddress);
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @GET
    @Path("cards")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserPaymentInfo> getCards() {
        List<UserPaymentInfo> cards = null;
        
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            String username = (String) session.getAttribute("logged");
            
            if (username != null) {
                EntityManager em = EMF.getEntityManager();
                Query q = em.createNamedQuery("Users.findByUsername");
                q.setParameter("username", username);
                List<Users> resultList = q.getResultList();
                
                if (!resultList.isEmpty()) {
                    Query q1 = em.createQuery("SELECT upi FROM UserPaymentInfo upi WHERE upi.userId = :userId");
                    q1.setParameter("userId", resultList.get(0));
                    cards = q1.getResultList();
                }
                
                EMF.returnEntityManager(em);
            }
        }
        
        return cards;
    }
    
    @GET
    @Path("cards/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserPaymentInfo getCard(@PathParam("id") Integer id) {
        EntityManager em = EMF.getEntityManager();
        UserPaymentInfo foundCard = em.find(UserPaymentInfo.class, id);
        EMF.returnEntityManager(em);
        
        return foundCard;
    }
    
    @POST
    @Path("cards")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status addCard(UserPaymentInfo card) throws Exception {
        Status status = new Status();
        status.setStatus("failed");
        status.setMessage("User not found!");
        
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            String username = (String) session.getAttribute("logged");
            
            if (username != null) {
                EntityManager em = EMF.getEntityManager();
                Query q = em.createNamedQuery("Users.findByUsername");
                q.setParameter("username", username);
                List<Users> resultList = q.getResultList();
                
                if (!resultList.isEmpty()) {
                    EntityTransaction tx = EMF.getTransaction(em);
                    card.setUserId(resultList.get(0));
                    em.persist(card);
                    EMF.commitTransaction(tx);
                    
                    status.setStatus("success");
                }
                
                EMF.returnEntityManager(em);
            }
        }
        
        return status;
    }
    
    @PUT
    @Path("cards/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status editCard(@PathParam("id") Integer id, UserPaymentInfo card) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        UserPaymentInfo foundCard = em.find(UserPaymentInfo.class, id);
        
        if (foundCard == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Card not found!");
            
            return status;
        }
        
        EntityTransaction tx = EMF.getTransaction(em);
        foundCard.setCardHolderFirstName(card.getCardHolderFirstName());
        foundCard.setCardHolderLastName(card.getCardHolderLastName());
        foundCard.setCardNumber(card.getCardNumber());
        foundCard.setCardExpiryDate(card.getCardExpiryDate());
        foundCard.setCardCvv(card.getCardCvv());
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @DELETE
    @Path("cards/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Status deleteCard(@PathParam("id") Integer id) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        UserPaymentInfo foundCard = em.find(UserPaymentInfo.class, id);
        
        if (foundCard == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Card not found!");
            
            return status;
        }
        
        EntityTransaction tx = EMF.getTransaction(em);
        em.remove(foundCard);
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @GET
    @Path("carts")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Carts> getCarts() {
        List<Carts> carts = null;
        
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            String username = (String) session.getAttribute("logged");
            
            if (username != null) {
                EntityManager em = EMF.getEntityManager();
                Query q = em.createNamedQuery("Users.findByUsername");
                q.setParameter("username", username);
                List<Users> resultList = q.getResultList();
                
                if (!resultList.isEmpty()) {
                    Query q1 = em.createQuery("SELECT c FROM Carts c WHERE c.userId = :userId AND c.datePurchased IS NOT NULL");
                    q1.setParameter("userId", resultList.get(0));
                    carts = q1.getResultList();
                    
                    for (Carts cart : carts) {
                        Query q2 = em.createQuery("SELECT cd FROM CartData cd WHERE cd.cartId = :cartId");
                        q2.setParameter("cartId", cart);
                        List<CartData> resultList1 = q2.getResultList();
                        cart.setCount(resultList1.size());
                    }
                }
                
                EMF.returnEntityManager(em);
            }
        }
        
        return carts;
    }
    
    @GET
    @Path("carts/data")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CartData> getCartInfo(@QueryParam("cid") Integer cartId) {
        EntityManager em = EMF.getEntityManager();
        Carts foundCart = em.find(Carts.class, cartId);
        Query q = em.createQuery("SELECT cd FROM CartData cd WHERE cd.cartId = :cartId");
        q.setParameter("cartId", foundCart);
        List<CartData> resultList = q.getResultList();
        EMF.returnEntityManager(em);
        
        return resultList;
    }
}