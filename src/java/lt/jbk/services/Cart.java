/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import lt.jbk.entities.CartData;
import lt.jbk.entities.Carts;
import lt.jbk.entities.Products;
import lt.jbk.entities.Users;
import lt.jbk.resources.EMF;
import lt.jbk.resources.Status;

/**
 *
 * @author Justi
 */
@Path("carts")
public class Cart {
    @Context HttpServletRequest request;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Carts> getCarts(@QueryParam("b") Boolean bought, @QueryParam("sort") String sort, @QueryParam("col") String col) {
        EntityManager em = EMF.getEntityManager();
        
        List<Carts> resultList = new ArrayList<>();
        
        if (bought == null) {
            Query q = em.createNamedQuery("Carts.findAll");
            resultList = q.getResultList();
        } else {
            if (bought) {
                Query q = em.createQuery("SELECT c FROM Carts c WHERE c.datePurchased IS NOT NULL");
                resultList = q.getResultList();
            }

            if (!bought) {
                Query q = em.createQuery("SELECT c FROM Carts c WHERE c.datePurchased IS NULL");
                resultList = q.getResultList();
            }
        }
        
        resultList.forEach((cart) -> {
            Query q1 = em.createQuery("SELECT cd FROM CartData cd WHERE cd.cartId = :cartId");
            q1.setParameter("cartId", cart);
            List<CartData> resultList1 = q1.getResultList();
            cart.setCount(resultList1.size());
        });
        
        EMF.returnEntityManager(em);
        
        if ("esc".equals(sort)) {
            if ("id".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getId().compareTo(y.getId())).collect(Collectors.toList());
            }
            
            if ("created".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getDateCreated().compareTo(y.getDateCreated())).collect(Collectors.toList());
            }
            
            if ("purchased".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getDatePurchased().compareTo(y.getDatePurchased())).collect(Collectors.toList());
            }
            
            if ("user".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getUserId().getUsername().compareTo(y.getUserId().getUsername())).collect(Collectors.toList());
            }
            
            if ("productcount".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getCount().compareTo(y.getCount())).collect(Collectors.toList());
            }
        }
        
        if ("desc".equals(sort)) {
            if ("id".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getId().compareTo(x.getId())).collect(Collectors.toList());
            }
            
            if ("created".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getDateCreated().compareTo(x.getDateCreated())).collect(Collectors.toList());
            }
            
            if ("purchased".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getDatePurchased().compareTo(x.getDatePurchased())).collect(Collectors.toList());
            }
            
            if ("user".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getUserId().getUsername().compareTo(x.getUserId().getUsername())).collect(Collectors.toList());
            }
            
            if ("productcount".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getCount().compareTo(x.getCount())).collect(Collectors.toList());
            }
        }
        
        return resultList;
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status addToCart(@QueryParam("pid") Integer productId, CartData product) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        HttpSession session = request.getSession();
        
        Integer cartId = (Integer) session.getAttribute("cartId");
        
        if (cartId == null) {
            EntityTransaction tx = EMF.getTransaction(em);
            String userName = (String) session.getAttribute("logged");
            
            Users user = null;
            
            if (userName != null) {
                Query q = em.createNamedQuery("Users.findByUsername");
                q.setParameter("username", userName);
                List<Users> resultList = q.getResultList();
                
                if (!resultList.isEmpty()) {
                    user = resultList.get(0);
                }
            }
            
            Carts cart = new Carts();
            cart.setDateCreated(new Date());
            cart.setDatePurchased(null);
            cart.setUserId(user);
            em.persist(cart);
            EMF.commitTransaction(tx);
            
            session.setAttribute("cartId", cart.getId());
        }
        
        cartId = (Integer) session.getAttribute("cartId");
        
        Carts foundCart = em.find(Carts.class, cartId);
        
        if (foundCart == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Cart not found!");
            
            return status;
        }
        
        Products foundProduct = em.find(Products.class, productId);
        
        if (foundProduct == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Product not found!");
            
            return status;
            
        }
        
        if (product.getQuantity() > foundProduct.getQuantity()) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Too many quantity!");
            
            return status;
        }
        
        Query q = em.createQuery("SELECT cd FROM CartData cd WHERE cd.cartId = :cartId AND cd.productId = :productId AND cd.info = :info");
        q.setParameter("cartId", foundCart);
        q.setParameter("productId", foundProduct);
        q.setParameter("info", product.getInfo());
        List<CartData> resultList = q.getResultList();
        
        if (resultList.isEmpty()) {
            EntityTransaction txx = EMF.getTransaction(em);
            product.setCartId(foundCart);
            product.setProductId(foundProduct);
            product.setPrice(foundProduct.getPrice().multiply(BigDecimal.valueOf(product.getQuantity())));
            em.persist(product);
            EMF.commitTransaction(txx);
        } else {
            EntityTransaction txx = EMF.getTransaction(em);
            resultList.get(0).setQuantity(resultList.get(0).getQuantity() + product.getQuantity());
            resultList.get(0).setPrice(foundProduct.getPrice().multiply(BigDecimal.valueOf(resultList.get(0).getQuantity())));
            EMF.commitTransaction(txx);
        }
        
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @GET
    @Path("adata")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CartData> getCartProducts(@QueryParam("cid") Integer cartId) {
        EntityManager em = EMF.getEntityManager();
        Query q = em.createQuery("SELECT cd FROM CartData cd WHERE cd.cartId = :cartId");
        q.setParameter("cartId", em.find(Carts.class, cartId));
        List<CartData> resultList = q.getResultList();
        EMF.returnEntityManager(em);
        
        return resultList;
    }
    
    @GET
    @Path("data")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CartData> getCartData() {
        HttpSession session = request.getSession();
        
        if (session != null) {
            Integer cartId = (Integer) session.getAttribute("cartId");
            
            if (cartId != null) {
                EntityManager em = EMF.getEntityManager();
                Query q = em.createQuery("SELECT cd FROM CartData cd WHERE cd.cartId = :cartId");
                Carts foundCart = em.find(Carts.class, cartId);
                q.setParameter("cartId", foundCart);
                List<CartData> resultList = q.getResultList();
                EMF.returnEntityManager(em);
                
                return resultList;
            }
        }
        
        return null;
    }
    
    @PUT
    @Path("data/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status editQuantity(@PathParam("id") Integer id, CartData cartData) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        CartData foundCartData = em.find(CartData.class, id);
        
        if (foundCartData == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Product in cart not found!");
            
            return status;
        }
        
        Products foundProduct = em.find(Products.class, foundCartData.getProductId().getId());
        
        if (foundProduct == null) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Product not found!");
            
            return status;
        }
        
        if (foundProduct.getQuantity() < cartData.getQuantity()) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Quantity too many!");
            
            return status;
        }
        
        EntityTransaction tx = EMF.getTransaction(em);
        foundCartData.setQuantity(cartData.getQuantity());
        foundCartData.setPrice(BigDecimal.valueOf(cartData.getQuantity()).multiply(foundProduct.getPrice()));
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @DELETE
    @Path("data/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Status deleteCartData(@PathParam("id") Integer id) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        
        CartData foundCartData = em.find(CartData.class, id);
        Carts foundCart = foundCartData.getCartId();
        
        EntityTransaction tx = EMF.getTransaction(em);
        em.remove(foundCartData);
        EMF.commitTransaction(tx);
        
        Query q = em.createQuery("SELECT cd FROM CartData cd WHERE cd.cartId = :cartId");
        q.setParameter("cartId", foundCart);
        List<CartData> resultList = q.getResultList();
        
        if (resultList.isEmpty()) {
            EntityTransaction txx = EMF.getTransaction(em);
            em.remove(foundCart);
            EMF.commitTransaction(txx);
        }
        
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @GET
    @Path("buy")
    @Produces(MediaType.APPLICATION_JSON)
    public Status buy() throws Exception {
        Status status = new Status();
        status.setStatus("failed");
        status.setMessage("Cart not found!");
        
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            Integer cartId = (Integer) session.getAttribute("cartId");
            
            if (cartId != null) {
                EntityManager em = EMF.getEntityManager();
                Carts foundCart = em.find(Carts.class, cartId);
                
                if (foundCart != null) {
                    Query q = em.createQuery("SELECT cd FROM CartData cd WHERE cd.cartId = :cartId");
                    q.setParameter("cartId", foundCart);
                    List<CartData> resultList = q.getResultList();
                    
                    EntityTransaction tx = EMF.getTransaction(em);
                    
                    for (CartData cartData : resultList) {
                        Products foundProduct = em.find(Products.class, cartData.getProductId().getId());
                        
                        if (foundProduct.getQuantity() < cartData.getQuantity()) {
                            status.setMessage("Too many quantity!");
                            
                            break;
                        } else {
                            foundProduct.setQuantity(foundProduct.getQuantity() - cartData.getQuantity());
                        }
                    }
                    
                    if (!"Too many quantity!".equals(status.getMessage())) {
                        foundCart.setDatePurchased(new Date());
                        EMF.commitTransaction(tx);
                        
                        status.setStatus("success");
                        
                        session.setAttribute("cartId", null);
                    }
                }
                
                EMF.returnEntityManager(em);
            }
        }
        
        return status;
    }
}