/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.services;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import lt.jbk.entities.Products;
import lt.jbk.entities.ReceivingData;
import lt.jbk.entities.Receivings;
import lt.jbk.resources.EMF;
import lt.jbk.resources.Status;
import lt.jbk.wrappers.NewReceiving;
import lt.jbk.wrappers.ReceivingProduct;

/**
 *
 * @author Justi
 */
@Path("receivings")
public class Receiving {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Receivings> getReceivings(@QueryParam("sort") String sort, @QueryParam("col") String col) {
        EntityManager em = EMF.getEntityManager();
        Query q = em.createNamedQuery("Receivings.findAll");
        List<Receivings> resultList = q.getResultList();
        
        resultList.forEach((receiving) -> {
            Query q1 = em.createQuery("SELECT rd FROM ReceivingData rd WHERE rd.receivingId = :rid");
            q1.setParameter("rid", receiving);
            List resultList1 = q1.getResultList();
            
            receiving.setCount(resultList1.size());
        });
        
        EMF.returnEntityManager(em);
        
        if ("esc".equals(sort)) {
            if ("number".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getDocumentNumber().compareTo(y.getDocumentNumber())).collect(Collectors.toList());
            }
            
            if ("date".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getReceivingDate().compareTo(y.getReceivingDate())).collect(Collectors.toList());
            }
            
            if ("count".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getCount().compareTo(y.getCount())).collect(Collectors.toList());
            }
        }
        
        if ("desc".equals(sort)) {
            if ("number".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getDocumentNumber().compareTo(x.getDocumentNumber())).collect(Collectors.toList());
            }
            
            if ("date".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getReceivingDate().compareTo(x.getReceivingDate())).collect(Collectors.toList());
            }
            
            if ("count".equals(col)) { 
                resultList = resultList.stream().sorted((x, y) -> y.getCount().compareTo(x.getCount())).collect(Collectors.toList());
            }
        }
        
        return resultList;
    }
    
    @POST
    @Path("data")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status addReceiving(NewReceiving receivingData) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        EntityTransaction tx = EMF.getTransaction(em);
        Receivings newReceiving = new Receivings();
        newReceiving.setDocumentNumber(receivingData.getDocumentNumber());
        newReceiving.setReceivingDate(new Date());
        em.persist(newReceiving);
        EMF.commitTransaction(tx);
        
        EntityTransaction txx = EMF.getTransaction(em);
        
        for (ReceivingProduct product : receivingData.getProducts()) {
            Products foundProduct = em.find(Products.class, product.getProductId());
            
            if (foundProduct == null) {
                EMF.rollbackTransaction(txx);
                EMF.returnEntityManager(em);
                
                status.setStatus("failed");
                
                return status;
            }
            
            foundProduct.setQuantity(foundProduct.getQuantity() + product.getQuantity());
            ReceivingData receivingProduct = new ReceivingData();
            receivingProduct.setProductId(foundProduct);
            receivingProduct.setQuantity(product.getQuantity());
            receivingProduct.setReceivingId(newReceiving);
            em.persist(receivingProduct);
        }
        
        EMF.commitTransaction(txx);
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Receivings getReceiving(@PathParam("id") Integer id) {
        EntityManager em = EMF.getEntityManager();
        Receivings foundReceiving = em.find(Receivings.class, id);
        EMF.returnEntityManager(em);
        
        return foundReceiving;
    }
    
    @GET
    @Path("data")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ReceivingData> getReceivingData(@QueryParam("rid") Integer receivingId, @QueryParam("sort") String sort, @QueryParam("col") String col) {
        EntityManager em = EMF.getEntityManager();
        Query q = em.createQuery("SELECT rd FROM ReceivingData rd WHERE rd.receivingId = :rid");
        q.setParameter("rid", em.find(Receivings.class, receivingId));
        List<ReceivingData> resultList = q.getResultList();
        EMF.returnEntityManager(em);
        
        if ("esc".equals(sort)) {
            if ("product".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getProductId().getName().compareTo(y.getProductId().getName())).collect(Collectors.toList());
            }
            
            if ("quantity".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> Long.compare(x.getQuantity(), y.getQuantity())).collect(Collectors.toList());
            }
            
            if ("category".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> x.getProductId().getCategoryId().getName().compareTo(y.getProductId().getCategoryId().getName())).collect(Collectors.toList());
            }
        }
        
        if ("desc".equals(sort)) {
            if ("product".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getProductId().getName().compareTo(x.getProductId().getName())).collect(Collectors.toList());
            }
            
            if ("quantity".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> Long.compare(y.getQuantity(), x.getQuantity())).collect(Collectors.toList());
            }
            
            if ("category".equals(col)) {
                resultList = resultList.stream().sorted((x, y) -> y.getProductId().getCategoryId().getName().compareTo(x.getProductId().getCategoryId().getName())).collect(Collectors.toList());
            }
        }
        
        return resultList;
    }
}