/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.services;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import lt.jbk.entities.CartData;
import lt.jbk.entities.Carts;
import lt.jbk.entities.Users;
import lt.jbk.resources.BCrypt;
import lt.jbk.resources.EMF;
import lt.jbk.resources.Status;

/**
 *
 * @author Justi
 */
@Path("users")
public class Validation {
    @Context HttpServletRequest request;
    
    @GET
    @Path("logged")
    @Produces(MediaType.APPLICATION_JSON)
    public Status isLogged() {
        Status status = new Status();
        
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            String logged = (String) session.getAttribute("logged");
            
            if (logged != null) {
                status.setStatus("logged");
                status.setMessage(logged);
                
                return status;
            }
        }
        
        status.setStatus("Not logged");
        
        return status;
    }
    
    @GET
    @Path("isadmin")
    @Produces(MediaType.APPLICATION_JSON)
    public Status isadmin() {
        Status status = new Status();
        
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            Boolean isAdmin = (Boolean) session.getAttribute("isadmin");
            
            if (isAdmin != null && isAdmin) {
                status.setStatus("admin");
                
                return status;
            }
        }
        
        status.setStatus("not");
        
        return status;
    }
    
    @POST
    @Path("register")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status register(Users user) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        Query q = em.createNamedQuery("Users.findByUsername");
        q.setParameter("username", user.getUsername());
        List<Users> resultList = q.getResultList();
        
        if (!resultList.isEmpty()) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Username already exicts!");
            
            return status;
        }
        
        EntityTransaction tx = EMF.getTransaction(em);
        user.setDateCreated(new Date());
        user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
        em.persist(user);
        EMF.commitTransaction(tx);
        
        HttpSession session = request.getSession();
        
        session.setAttribute("logged", user.getUsername());
        session.setAttribute("isadmin", false);
        
        Integer cartId = (Integer) session.getAttribute("cartId");
        
        if (cartId != null) {
            EntityTransaction txx = EMF.getTransaction(em);
            Carts foundCart = em.find(Carts.class, cartId);
            
            if (foundCart != null) {
                foundCart.setUserId(user);
            }
            
            EMF.commitTransaction(txx);
        }
        
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Status login(Users user) throws Exception {
        Status status = new Status();
        
        EntityManager em = EMF.getEntityManager();
        Query q = em.createNamedQuery("Users.findByUsername");
        q.setParameter("username", user.getUsername());
        List<Users> resultList = q.getResultList();
        
        if (resultList.isEmpty()) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Username don't exist!");
            
            return status;
        }
        
        if (!BCrypt.checkpw(user.getPassword(), resultList.get(0).getPassword())) {
            EMF.returnEntityManager(em);
            
            status.setStatus("failed");
            status.setMessage("Wrong password!");
            
            return status;
        }
        
        HttpSession session = request.getSession();
        session.setAttribute("logged", user.getUsername());
        session.setAttribute("isadmin", (resultList.get(0).getIsAdmin() != 0));
        
        Integer cartId = (Integer) session.getAttribute("cartId");
        
        if (cartId != null) {
            Query q1 = em.createQuery("SELECT c FROM Carts c WHERE c.userId = :user AND c.datePurchased IS NULL");
            q1.setParameter("user", resultList.get(0));
            List<Carts> resultList1 = q1.getResultList();
            Carts foundCart = em.find(Carts.class, cartId);
            
            if (resultList1.isEmpty()) {
                if (foundCart != null) {
                    EntityTransaction txx = EMF.getTransaction(em);
                    foundCart.setUserId(resultList.get(0));
                    EMF.commitTransaction(txx);
                }
            } else {
                EntityTransaction tx = EMF.getTransaction(em);
                Query q2 = em.createQuery("SELECT cd FROM CartData cd WHERE cd.cartId = :cartId");
                q2.setParameter("cartId", foundCart);
                List<CartData> resultList2 = q2.getResultList();
                
                resultList2.forEach((cartData) -> {
                    Query q3 = em.createQuery("SELECT cd FROM CartData cd WHERE cd.cartId = :cartId AND cd.productId = :productId AND cd.info = :info");
                    q3.setParameter("cartId", resultList1.get(resultList1.size() - 1));
                    q3.setParameter("productId", cartData.getProductId());
                    q3.setParameter("info", cartData.getInfo());
                    List<CartData> resultList3 = q3.getResultList();
                    
                    if (resultList3.isEmpty()) {
                        cartData.setCartId(resultList1.get(resultList1.size() - 1));
                    } else {
                        resultList3.get(0).setQuantity(resultList3.get(0).getQuantity() + cartData.getQuantity());
                        resultList3.get(0).setPrice(resultList3.get(0).getPrice().add(cartData.getPrice()));
                    }
                });
                
                session.setAttribute("cartId", resultList1.get(resultList1.size() - 1).getId());
                
                em.remove(foundCart);
                EMF.commitTransaction(tx);
            }
        } else {
            Query q1 = em.createQuery("SELECT c FROM Carts c WHERE c.userId = :user AND c.datePurchased IS NULL");
            q1.setParameter("user", resultList.get(0));
            List<Carts> resultList1 = q1.getResultList();
            
            if (!resultList1.isEmpty()) {
                session.setAttribute("cartId", resultList1.get(resultList1.size() - 1).getId());
            }
        }
        
        EMF.returnEntityManager(em);
        
        status.setStatus("success");
        
        return status;
    }
    
    @GET
    @Path("logout")
    public void logout() {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
    }
}