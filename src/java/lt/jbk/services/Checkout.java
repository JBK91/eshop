/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import lt.jbk.resources.Status;

/**
 *
 * @author Justi
 */
@Path("checkout")
public class Checkout {
    private static String checkout = "info";
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Status getCheckout() {
        Status status = new Status();
        status.setStatus(checkout);
        
        return status;
    }
    
    @GET
    @Path("address")
    public void setAddress() {
        checkout = "address";
    }
    
    @GET
    @Path("cards")
    public void setCards() {
        checkout = "cards";
    }
    
    @GET
    @Path("info")
    public void setInfo() {
        checkout = "cards";
    }
}