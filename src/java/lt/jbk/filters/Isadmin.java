/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Justi
 */
@WebFilter(filterName = "Isadmin", urlPatterns = "/admin/*")
public class Isadmin implements Filter {
    @Override
    public void init(FilterConfig fc) throws ServletException {}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain fc) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) request).getSession(false);
        
        if (session == null || !(Boolean) session.getAttribute("isadmin")) {
            ((HttpServletResponse) response).sendRedirect("/eShop/index.html");
        } else {
            fc.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {} 
}