/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.wrappers;

import java.util.List;

/**
 *
 * @author Justi
 */
public class NewReceiving {
    private String documentNumber;
    private List<ReceivingProduct> products;

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public List<ReceivingProduct> getProducts() {
        return products;
    }

    public void setProducts(List<ReceivingProduct> products) {
        this.products = products;
    }
}