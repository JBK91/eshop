/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Justi
 */
@Entity
@Table(name = "user_payment_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserPaymentInfo.findAll", query = "SELECT u FROM UserPaymentInfo u")
    , @NamedQuery(name = "UserPaymentInfo.findById", query = "SELECT u FROM UserPaymentInfo u WHERE u.id = :id")
    , @NamedQuery(name = "UserPaymentInfo.findByCardNumber", query = "SELECT u FROM UserPaymentInfo u WHERE u.cardNumber = :cardNumber")
    , @NamedQuery(name = "UserPaymentInfo.findByCardExpiryDate", query = "SELECT u FROM UserPaymentInfo u WHERE u.cardExpiryDate = :cardExpiryDate")
    , @NamedQuery(name = "UserPaymentInfo.findByCardCvv", query = "SELECT u FROM UserPaymentInfo u WHERE u.cardCvv = :cardCvv")
    , @NamedQuery(name = "UserPaymentInfo.findByCardHolderFirstName", query = "SELECT u FROM UserPaymentInfo u WHERE u.cardHolderFirstName = :cardHolderFirstName")
    , @NamedQuery(name = "UserPaymentInfo.findByCardHolderLastName", query = "SELECT u FROM UserPaymentInfo u WHERE u.cardHolderLastName = :cardHolderLastName")
    , @NamedQuery(name = "UserPaymentInfo.findByPaypalEmail", query = "SELECT u FROM UserPaymentInfo u WHERE u.paypalEmail = :paypalEmail")})
public class UserPaymentInfo implements Serializable {
    private static final long serialVersionUID = 9L;
    
    private Integer id;
    private String cardNumber;
    private String cardExpiryDate;
    private String cardCvv;
    private String cardHolderFirstName;
    private String cardHolderLastName;
    private String paypalEmail;
    private Users userId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "card_number")
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "card_expiry_date")
    public String getCardExpiryDate() {
        return cardExpiryDate;
    }

    public void setCardExpiryDate(String cardExpiryDate) {
        this.cardExpiryDate = cardExpiryDate;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "card_cvv")
    public String getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(String cardCvv) {
        this.cardCvv = cardCvv;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "card_holder_first_name")
    public String getCardHolderFirstName() {
        return cardHolderFirstName;
    }

    public void setCardHolderFirstName(String cardHolderFirstName) {
        this.cardHolderFirstName = cardHolderFirstName;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "card_holder_last_name")
    public String getCardHolderLastName() {
        return cardHolderLastName;
    }

    public void setCardHolderLastName(String cardHolderLastName) {
        this.cardHolderLastName = cardHolderLastName;
    }

    @Size(max = 255)
    @Column(name = "paypal_email")
    public String getPaypalEmail() {
        return paypalEmail;
    }

    public void setPaypalEmail(String paypalEmail) {
        this.paypalEmail = paypalEmail;
    }

    @JoinColumn(name = "user_id", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "UserPaymentInfo{" + "id=" + id + ", cardNumber=" + cardNumber + ", cardExpiryDate=" + cardExpiryDate + ", cardCvv=" + cardCvv + ", cardHolderFirstName=" + cardHolderFirstName + ", cardHolderLastName=" + cardHolderLastName + ", paypalEmail=" + paypalEmail + ", userId=" + userId + '}';
    }
}