/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Justi
 */
@Entity
@Table(name = "users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u")
    , @NamedQuery(name = "Users.findById", query = "SELECT u FROM Users u WHERE u.id = :id")
    , @NamedQuery(name = "Users.findByUsername", query = "SELECT u FROM Users u WHERE u.username = :username")
    , @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password")
    , @NamedQuery(name = "Users.findByEmail", query = "SELECT u FROM Users u WHERE u.email = :email")
    , @NamedQuery(name = "Users.findByDateCreated", query = "SELECT u FROM Users u WHERE u.dateCreated = :dateCreated")
    , @NamedQuery(name = "Users.findByIsAdmin", query = "SELECT u FROM Users u WHERE u.isAdmin = :isAdmin")})
public class Users implements Serializable {
    private static final long serialVersionUID = 10L;
    
    private Integer id;
    private String username;
    private String password;
    private String email;
    private Date dateCreated;
    private short isAdmin;
    private Set<UserPaymentInfo> userPaymentInfoSet;
    private Set<Carts> cartsSet;
    private Set<UserInfo> userInfoSet;
    private Set<UserAddresses> userAddressesSet;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "date_created")
    @Temporal(TemporalType.DATE)
    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        if (dateCreated != null) {
            this.dateCreated = new Date(dateCreated.getTime());
        } else {
            this.dateCreated = null;
        }
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_admin")
    public short getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(short isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    public Set<UserPaymentInfo> getUserPaymentInfoSet() {
        return userPaymentInfoSet;
    }

    public void setUserPaymentInfoSet(Set<UserPaymentInfo> userPaymentInfoSet) {
        this.userPaymentInfoSet = userPaymentInfoSet;
    }

    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    public Set<Carts> getCartsSet() {
        return cartsSet;
    }

    public void setCartsSet(Set<Carts> cartsSet) {
        this.cartsSet = cartsSet;
    }

    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    public Set<UserInfo> getUserInfoSet() {
        return userInfoSet;
    }

    public void setUserInfoSet(Set<UserInfo> userInfoSet) {
        this.userInfoSet = userInfoSet;
    }

    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    public Set<UserAddresses> getUserAddressesSet() {
        return userAddressesSet;
    }

    public void setUserAddressesSet(Set<UserAddresses> userAddressesSet) {
        this.userAddressesSet = userAddressesSet;
    }

    @Override
    public String toString() {
        return "Users{" + "id=" + id + ", username=" + username + ", password=" + password + ", email=" + email + ", dateCreated=" + dateCreated + ", isAdmin=" + isAdmin + ", userPaymentInfoSet=" + userPaymentInfoSet + ", cartsSet=" + cartsSet + ", userInfoSet=" + userInfoSet + ", userAddressesSet=" + userAddressesSet + '}';
    }
}