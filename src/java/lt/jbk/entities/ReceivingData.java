/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Justi
 */
@Entity
@Table(name = "receiving_data")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReceivingData.findAll", query = "SELECT r FROM ReceivingData r")
    , @NamedQuery(name = "ReceivingData.findById", query = "SELECT r FROM ReceivingData r WHERE r.id = :id")
    , @NamedQuery(name = "ReceivingData.findByQuantity", query = "SELECT r FROM ReceivingData r WHERE r.quantity = :quantity")})
public class ReceivingData implements Serializable {
    private static final long serialVersionUID = 5L;
    
    private Integer id;
    private long quantity;
    private Receivings receivingId;
    private Products productId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    @JoinColumn(name = "receiving_id", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    public Receivings getReceivingId() {
        return receivingId;
    }

    public void setReceivingId(Receivings receivingId) {
        this.receivingId = receivingId;
    }

    @JoinColumn(name = "product_id", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    public Products getProductId() {
        return productId;
    }

    public void setProductId(Products productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "ReceivingData{" + "id=" + id + ", quantity=" + quantity + ", receivingId=" + receivingId + ", productId=" + productId + '}';
    }
}