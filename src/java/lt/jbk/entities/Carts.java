/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Justi
 */
@Entity
@Table(name = "carts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Carts.findAll", query = "SELECT c FROM Carts c")
    , @NamedQuery(name = "Carts.findById", query = "SELECT c FROM Carts c WHERE c.id = :id")
    , @NamedQuery(name = "Carts.findByDateCreated", query = "SELECT c FROM Carts c WHERE c.dateCreated = :dateCreated")
    , @NamedQuery(name = "Carts.findByDatePurchased", query = "SELECT c FROM Carts c WHERE c.datePurchased = :datePurchased")})
public class Carts implements Serializable {
    private static final long serialVersionUID = 2L;
    
    private Integer id;
    private Date dateCreated;
    private Date datePurchased;
    private Integer count;
    private Set<CartData> cartDataSet;
    private Users userId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "date_created")
    @Temporal(TemporalType.DATE)
    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        if (dateCreated != null) {
            this.dateCreated = new Date(dateCreated.getTime());
        } else {
            this.dateCreated = null;
        }
    }

    @Column(name = "date_purchased")
    @Temporal(TemporalType.DATE)
    public Date getDatePurchased() {
        return datePurchased;
    }

    public void setDatePurchased(Date datePurchased) {
        if (datePurchased != null) {
            this.datePurchased = new Date(datePurchased.getTime());
        } else {
            this.datePurchased = null;
        }
    }

    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cartId")
    public Set<CartData> getCartDataSet() {
        return cartDataSet;
    }

    public void setCartDataSet(Set<CartData> cartDataSet) {
        this.cartDataSet = cartDataSet;
    }

    @JoinColumn(name = "user_id", referencedColumnName = "ID")
    @ManyToOne
    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Transient
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Carts{" + "id=" + id + ", dateCreated=" + dateCreated + ", datePurchased=" + datePurchased + ", count=" + count + ", cartDataSet=" + cartDataSet + ", userId=" + userId + '}';
    }
}