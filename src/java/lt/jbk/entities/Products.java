/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Justi
 */
@Entity
@Table(name = "products")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Products.findAll", query = "SELECT p FROM Products p")
    , @NamedQuery(name = "Products.findById", query = "SELECT p FROM Products p WHERE p.id = :id")
    , @NamedQuery(name = "Products.findByQuantity", query = "SELECT p FROM Products p WHERE p.quantity = :quantity")
    , @NamedQuery(name = "Products.findByPrice", query = "SELECT p FROM Products p WHERE p.price = :price")
    , @NamedQuery(name = "Products.findByName", query = "SELECT p FROM Products p WHERE p.name = :name")
    , @NamedQuery(name = "Products.findByDescription", query = "SELECT p FROM Products p WHERE p.description = :description")})
public class Products implements Serializable {
    private static final long serialVersionUID = 4L;
    
    private Integer id;
    private long quantity;
    private BigDecimal price;
    private String description;
    private String name;
    private String image;
    private Set<CartData> cartDataSet;
    private Set<ReceivingData> receivingDataSet;
    private Categories categoryId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Size(max = 255)
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Size(max = 255)
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "image")
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId")
    public Set<CartData> getCartDataSet() {
        return cartDataSet;
    }

    public void setCartDataSet(Set<CartData> cartDataSet) {
        this.cartDataSet = cartDataSet;
    }

    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId")
    public Set<ReceivingData> getReceivingDataSet() {
        return receivingDataSet;
    }

    public void setReceivingDataSet(Set<ReceivingData> receivingDataSet) {
        this.receivingDataSet = receivingDataSet;
    }

    @JoinColumn(name = "category_id", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    public Categories getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Categories categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Products{" + "id=" + id + ", quantity=" + quantity + ", price=" + price + ", description=" + description + ", cartDataSet=" + cartDataSet + ", receivingDataSet=" + receivingDataSet + ", categoryId=" + categoryId + '}';
    }
}