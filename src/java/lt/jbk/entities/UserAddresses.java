/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Justi
 */
@Entity
@Table(name = "user_addresses")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserAddresses.findAll", query = "SELECT u FROM UserAddresses u")
    , @NamedQuery(name = "UserAddresses.findById", query = "SELECT u FROM UserAddresses u WHERE u.id = :id")
    , @NamedQuery(name = "UserAddresses.findByAddress", query = "SELECT u FROM UserAddresses u WHERE u.address = :address")
    , @NamedQuery(name = "UserAddresses.findByCity", query = "SELECT u FROM UserAddresses u WHERE u.city = :city")
    , @NamedQuery(name = "UserAddresses.findByCountry", query = "SELECT u FROM UserAddresses u WHERE u.country = :country")
    , @NamedQuery(name = "UserAddresses.findByPostCode", query = "SELECT u FROM UserAddresses u WHERE u.postCode = :postCode")})
public class UserAddresses implements Serializable {
    private static final long serialVersionUID = 7L;
    
    private Integer id;
    private String address;
    private String city;
    private String country;
    private String postCode;
    private Users userId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "country")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "post_code")
    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @JoinColumn(name = "user_id", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "UserAddresses{" + "id=" + id + ", address=" + address + ", city=" + city + ", country=" + country + ", postCode=" + postCode + ", userId=" + userId + '}';
    }
}