/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Justi
 */
@Entity
@Table(name = "cart_data")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CartData.findAll", query = "SELECT c FROM CartData c")
    , @NamedQuery(name = "CartData.findById", query = "SELECT c FROM CartData c WHERE c.id = :id")
    , @NamedQuery(name = "CartData.findByQuantity", query = "SELECT c FROM CartData c WHERE c.quantity = :quantity")
    , @NamedQuery(name = "CartData.findByPrice", query = "SELECT c FROM CartData c WHERE c.price = :price")})
public class CartData implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private long quantity;
    private BigDecimal price;
    private String info;
    private Products productId;
    private Carts cartId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @JoinColumn(name = "product_id", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    public Products getProductId() {
        return productId;
    }

    public void setProductId(Products productId) {
        this.productId = productId;
    }

    @JoinColumn(name = "cart_id", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    public Carts getCartId() {
        return cartId;
    }

    public void setCartId(Carts cartId) {
        this.cartId = cartId;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "info")
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "CartData{" + "id=" + id + ", quantity=" + quantity + ", price=" + price + ", info=" + info + ", productId=" + productId + ", cartId=" + cartId + '}';
    }
}