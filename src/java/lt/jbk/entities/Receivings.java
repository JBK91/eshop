/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jbk.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Justi
 */
@Entity
@Table(name = "receivings")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Receivings.findAll", query = "SELECT r FROM Receivings r")
    , @NamedQuery(name = "Receivings.findById", query = "SELECT r FROM Receivings r WHERE r.id = :id")
    , @NamedQuery(name = "Receivings.findByReceivingDate", query = "SELECT r FROM Receivings r WHERE r.receivingDate = :receivingDate")
    , @NamedQuery(name = "Receivings.findByDocumentNumber", query = "SELECT r FROM Receivings r WHERE r.documentNumber = :documentNumber")})
public class Receivings implements Serializable {
    private static final long serialVersionUID = 6L;
    
    private Integer id;
    private Date receivingDate;
    private String documentNumber;
    private Integer count;
    private Set<ReceivingData> receivingDataSet;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "receiving_date")
    @Temporal(TemporalType.DATE)
    public Date getReceivingDate() {
        return receivingDate;
    }

    public void setReceivingDate(Date receivingDate) {
        if (receivingDate != null) {
            this.receivingDate = new Date(receivingDate.getTime());
        } else {
            this.receivingDate = null;
        }
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "document_number")
    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    @Transient
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "receivingId")
    public Set<ReceivingData> getReceivingDataSet() {
        return receivingDataSet;
    }

    public void setReceivingDataSet(Set<ReceivingData> receivingDataSet) {
        this.receivingDataSet = receivingDataSet;
    }

    @Override
    public String toString() {
        return "Receivings{" + "id=" + id + ", receivingDate=" + receivingDate + ", documentNumber=" + documentNumber + ", receivingDataSet=" + receivingDataSet + '}';
    }
}