/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    request = $.ajax({url: "json/carts/data", type: "GET"});
    request.done(response => {
        $("#items").empty();

        let totalQuantity = 0;
        let totalPrice = 0;

        if (response === undefined) {
            window.location.href = "index.html";
        }

        for (let i = 0; i < response.length; i++) {
            let newTr = "<tr>";
            newTr += "<td>" + response[i].productId.name + "</td>";
            newTr += "<td>" + response[i].quantity + "</td>";
            newTr += "<td>" + response[i].price + "</td></tr>";

            $("#items").append(newTr);

            totalQuantity += response[i].quantity;
            totalPrice += response[i].price;
        }

        let infoTr = "<tr><td>Total:</td><td>" + totalQuantity + "</td><td>" + totalPrice.toFixed(2) + "</td></tr>";

        $("#items").append(infoTr);
    });

    request1 = $.ajax({url: "json/users/logged", type: "GET"});
    request1.done(response => {
        if (response.status === "logged") {
            request2 = $.ajax({url: "json/profile/info", type: "GET"});
            request2.done(response1 => {
                if (response1 === undefined) {
                    let infoDiv = "Please fill your info!<br>";
                    infoDiv += "<button class=\"btn btn-primary\" id=\"editInfoButton1\">Fill</button>";

                    $("#infoDiv").append(infoDiv);

                    $("#editInfoButton1").click(() => $("#editInfoModal").modal("show"));
                } else {
                    request6 = $.ajax({url: "json/checkout/address", type: "GET"});
                    request6.done(() => {
                        $("#infoDiv").empty();
                        showAddresses();
                    });
                }
            });
        } else {
            $("#infoDiv").text("Please login or register first!");
        }
    });
    
    request0 = $.ajax({url: "json/checkout", type: "GET"});
    request0.done(response => {
        if (response.status === "address") {
            $("#infoDiv").empty();
            showAddresses();
        }
        
        if (response.status === "cards") {
            $("#infoDiv").empty();
            showCards();
        }
    });
    
    function showAddresses() {
        request3 = $.ajax({url: "json/profile/address", type: "GET"});
        request3.done(response => {
            $("#infoDiv").empty();
            
            if (response.length === 0) {
                $("#infoDiv").empty();
                let addressDiv = "Please add at least 1 address in your profile!<br>";
                addressDiv += "<button class=\"btn btn-primary\" id=\"addAddress\">Add</button>";
                
                $("#infoDiv").append(addressDiv);
                
                $("#addAddress").click(() => $("#addAddressModal").modal("show"));
            } else {
                $("#infoDiv").empty();
                let newSelect = "<select id=\"address\">";
                
                for (let i = 0; i < response.length; i++) {
                    newSelect += "<option value=\"" + response[i].id + "\">";
                    newSelect += response[i].address + " ";
                    newSelect += response[i].city + " ";
                    newSelect += response[i].country + " ";
                    newSelect += response[i].postCode + "</option>";
                }
                
                newSelect += "</select>";
                
                $("#infoDiv").append(newSelect);
                
                let addBtn = "<button class=\"btn btn-primary\" id=\"addAddress1\">Add</button>";
                
                $("#infoDiv").append(addBtn);
                $("#infoDiv").append("<button class=\"btn btn-primary\" id=\"nextButton\">Next</button>");
                
                $("#addAddress1").click(() => $("#addAddressModal").modal("show"));
                $("#nextButton").click(() => {
                    request7 = $.ajax({url: "json/checkout/cards", type: "GET"});
                    request7.done(() => showCards());
                });
            }
        });
    }
    
    function showCards() {
        $("#infoDiv").empty();
        request4 = $.ajax({url: "json/profile/cards", type: "GET"});
        request4.done(response => {
            $("#infoDiv").empty();
            
            if (response.length === 0) {
                let cardsDiv = "Please add at least one card in profile!<br>";
                cardsDiv += "<button class=\"btn btn-primary\" id=\"addCard\">Add</button>";
                
                $("#infoDiv").append(cardsDiv);
            } else {
                let newSelect = "<select id=\"card\">";
                
                for (let i = 0; i < response.length; i++) {
                    newSelect += "<option value=\"" + response[i].id + "\">";
                    newSelect += response[i].cardHolderFirstName + " ";
                    newSelect += response[i].cardHolderLastName + " ";
                    newSelect += response[i].cardNumber + "</option>";
                }
                
                newSelect += "</select>";
                
                $("#infoDiv").append(newSelect);
                
                let addBtn = "<button class=\"btn btn-primary\" id=\"addCard\">Add</button>";
                
                $("#infoDiv").append(addBtn);
                $("#infoDiv").append("<button class=\"btn btn-primary\" id=\"buyButton\">Buy</button>");
                $("#buyButton").click(() => {
                    request8 = $.ajax({url: "json/checkout/info", type: "GET"});
                    request8.done(() => buy());
                });
            }
            
            $("#addCard").click(() => $("#addCardModal").modal("show"));
        });
    }
    
    function buy() {
        request5 = $.ajax({url: "json/carts/buy"});
        request5.done(response => {
            if (response.status === "failed") {
                alert(response.message);
            }
            
            if (response.status === "success") {
                window.location.href = "profile/purchases.html";
            }
        });
    }
});