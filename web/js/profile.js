/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    $("#navigation").empty();
    
    let nav = "<ul>";
    nav += "<li class=\"list-group-item\"><a href=index.html class=\"btn btn-primary\">Info</button></li>";
    nav += "<li class=\"list-group-item\"><a href=addresses.html class=\"btn btn-primary\">Addresses</button></li>";
    nav += "<li class=\"list-group-item\"><a href=cards.html class=\"btn btn-primary\">Cards</button></li>";
    nav += "<li class=\"list-group-item\"><a href=purchases.html class=\"btn btn-primary\">Purchase history</button></li>";
    nav += "</ul>";
    
    $("#navigation").append(nav);
    
    request = $.ajax({url: "/eShop/json/profile", type: "GET"});
    request.done(response => {
        $("#profile").text(response.username + " profile");
        $("#email").text(response.email);
        
        $("#editEmail").val(response.email);
    });
    
    request1 = $.ajax({url: "/eShop/json/profile/info"});
    request1.done(response => {
        if (response !== undefined) {
            $("#firstName").text(response.firstName);
            $("#lastName").text(response.lastName);
            $("#phone").text(response.phoneNumber);
            
            $("#editFirstName").val(response.firstName);
            $("#editLastName").val(response.lastName);
            $("#editPhone").val(response.phoneNumber);
        }
    });
    
    $("#editInfoButton").click(() => $("#editInfoModal").modal("show"));
    
    $("#editInfo").click(() => {
        let firstName = $("#editFirstName").val();
        let lastName = $("#editLastName").val();
        let email = $("#editEmail").val();
        let phone = $("#editPhone").val();
        try {
            if (!firstName) throw "Enter first name!";
            if (!lastName) throw "Enter last name!";
            if (!email) throw "Enter email!";
            if (!phone) throw "Enter phone!"; 
            if (firstName.match(/[\W_]+/)) throw "First name contains illegal charachters!";
            if (lastName.match(/[\W_]+/)) throw "Last name contains illegal charachters!";
            if (!email.match(/^[\w\d]+[@]\w+[.]\w+$/)) throw "Enter a valid email!";
            if (!phone.match(/^[+]*[\d]+$/)) throw "Enter a valid phone!";
            
            $(".editInfoError").text();
            $(".editInfoError").addClass("d-none");
            
            let data = {
                firstName: firstName,
                lastName: lastName,
                phoneNumber: phone
            };
            
            request2 = $.ajax({
                url: "/eShop/json/profile/info",
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });
            
            request2.done(response => {
                if (response.status === "failed") {
                    $(".editInfoError").text(response.message);
                    $(".editInfoError").removeClass("d-none");
                }
                
                if (response.status === "success") {
                    let data1 = {
                        email: email
                    };
                    
                    request3 = $.ajax({
                        url: "/eShop/json/profile",
                        type: "PUT",
                        data: JSON.stringify(data1),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json"
                    });
                    
                    request3.done(response1 => {
                        if (response1.status === "failed") {
                            $(".editInfoError").text(response.message);
                            $(".editInfoError").removeClass("d-none");
                        }
                        
                        if (response1.status === "success") {
                            $(".editInfoError").text("");
                            $(".editInfoError").addClass("d-none");
                            
                            $("#eidtInfoModal").modal("hide");
                            
                            window.location.reload();
                        }
                    });
                    
                    request3.fail(errorThrown1 => {
                        $(".editInfoError").text(errorThrown1);
                        $(".editInfoError").removeClass("d-none");
                    });
                }
            });
            
            request2.fail(errorThrown => {
                $(".editInfoError").text(errorThrown);
                $(".editInfoError").removeClass("d-none");
            });
        } catch (error) {
            $(".editInfoError").text(error);
            $(".editInfoError").removeClass("d-none");
        }
    });
    
    $("#addAddress").click(() => {
        let address = $("#address").val();
        let city = $("#city").val();
        let country = $("#country").val();
        let postcode = $("#postcode").val();
        
        try {
            if (!address) throw "Enter address!";
            if (!city) throw "Enter city!";
            if (!country) throw "Enter country!";
            if (!postcode) throw "Enter post code!";
            
            $(".addAddressError").text("");
            $(".addAddressError").addClass("d-none");
            
            let data = {
                address: address,
                city: city,
                country: country,
                postCode: postcode
            };
            
            request4 = $.ajax({
                url: "/eShop/json/profile/address",
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });
            
            request4.done(response => {
                if (response.status === "failed") {
                    $(".addAddressError").text(response.message);
                    $(".addAddressError").removeClass("d-none");
                }
                
                if (response.status === "success") {
                    $(".addAddressError").text("");
                    $(".addAddressError").addClass("d-none");
                    
                    $("#address").val("");
                    $("#city").val("");
                    $("#country").val("");
                    $("#postcode").val("");
                    
                    $("#addAddressModal").modal("hide");
                    
                    window.location.reload();
                }
            });
            
            request4.fail(errorThrown => {
                $(".addAddressError").text(errorThrown);
                $(".addAddressError").removeClass("d-none");
            });
        } catch (error) {
            $(".addAddressError").text(error);
            $(".addAddressError").removeClass("d-none");
        }
    });
    
    $("#addCardBtn").click(() => {
        let firstName = $("#cardFirstName").val();
        let lastName = $("#cardLastName").val();
        let cardNumber = $("#cardNumber").val();
        let cardDate = $("#cardDate").val();
        let cvv = $("#cardCvv").val();
        
        try {
            if (!firstName) throw "Enter first name!";
            if (!lastName) throw "Enter last name!";
            if (!cardNumber) throw "Enter card number!";
            if (!cardDate) throw "Enter card expiry date!";
            if (!cvv) throw "Enter card cvv!";
            if (firstName.match(/[\W_]+/)) throw "Enter valid first name!";
            if (lastName.match(/[\W_]+/)) throw "Enter valid last name!";
            if (!cardNumber.match(/^[\d ]+$/)) throw "Enter valid card number!";
            if (!cardDate.match(/^\d{2}[/]\d{2}$/)) throw "Enter valid card expiry date!";
            if (!cvv.match(/^\d{3}$/)) throw "Enter valid valid card cvv";
            
            $(".addCardError").text("");
            $(".addcardError").addClass("d-none");
            
            let data = {
                cardHolderFirstName: firstName,
                cardHolderLastName: lastName,
                cardNumber: cardNumber,
                cardExpiryDate: cardDate,
                cardCvv: cvv
            };
            
            request5 = $.ajax({
                url: "/eShop/json/profile/cards",
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });
            
            request5.done(response => {
                if (response.status === "failed") {
                    $(".addCardError").text(response.message);
                    $(".addCardError").removeClass("d-none");
                }
                
                if (response.status === "success") {
                    $(".addCardError").text("");
                    $(".addCardError").addClass("d-none");
                    
                    $("#cardFirstName").val("");
                    $("#cardLastName").val("");
                    $("#cardNumber").val("");
                    $("#cardDate").val("");
                    $("#cardCvv").val("");
                    
                    $("#addCardModal").modal("hide");
                    
                    window.location.reload();
                }
            });
            
            request5.fail(errorThrown => {
                $(".addCardError").text(errorThrown);
                $(".addCardError").removeClass("d-none");
            });
        } catch (error) {
            $(".addCardError").text(error);
            $(".addCardError").removeClass("d-none");
        }
    });
});