/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    let queryParams = new URLSearchParams(window.location.search);

    if (!queryParams.has("rid")) {
        window.location.href = "receivings.html";
    }

    let receivingId = queryParams.get("rid");

    if (isNaN(receivingId) || receivingId === "") {
        window.location.href = "receivings.html";
    }

    request = $.ajax({url: "/eShop/json/receivings/" + receivingId, type: "GET"});
    request.done(response => {
        if (response !== undefined) {
            $("#documentNumber").text(response.documentNumber);
            $("#receivingDate").text(response.receivingDate.split("T")[0]);
        } else {
            window.location.href = "receivings.html";
        }
    });

    request1 = $.ajax({url: "/eShop/json/receivings/data?rid=" + receivingId, type: "GET"});
    request1.done(response => {
        $("#tbody").empty();

        for (let i = 0; i < response.length; i++) {
            let newTr = "<tr>";
            newTr += "<td>" + response[i].productId.name + "</td>";
            newTr += "<td>" + response[i].quantity + "</td>";
            newTr += "<td>" + response[i].productId.categoryId.name + "</td>";
            newTr += "</tr>";

            $("#tbody").append(newTr);
        }
    });

    $(".sort-btn").click(e => {
        let sort = e.currentTarget.value;
        let column = e.currentTarget.id;

        $("#" + column).val(sort === "esc" ? "desc" : "esc");

        request2 = $.ajax({url: "/eShop/json/receivings/data?rid=" + receivingId + "&sort=" + sort + "&col=" + column, type: "GET"});
        request2.done(response => {
            $("#tbody").empty();

            for (let i = 0; i < response.length; i++) {
                let newTr = "<tr>";
                newTr += "<td>" + response[i].productId.name + "</td>";
                newTr += "<td>" + response[i].quantity + "</td>";
                newTr += "<td>" + response[i].productId.categoryId.name + "</td>";
                newTr += "</tr>";

                $("#tbody").append(newTr);
            }
        });
    });
});