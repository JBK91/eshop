/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    request = $.ajax({url: "/eShop/json/profile/cards", type: "GET"});
    request.done(response => {
        if (response !== undefined) {
            $("#tbody").empty();

            for (let i = 0; i < response.length; i++) {
                let newTr = "<tr>";
                newTr += "<td>" + response[i].cardHolderFirstName + " " + response[i].cardHolderLastName + "</td>";
                newTr += "<td>" + response[i].cardNumber + "</td>";
                newTr += "<td>" + response[i].cardExpiryDate + "</td>";
                newTr += "<td>" + response[i].cardCvv + "</td>";
                newTr += "<td class=\"d-flex\">";
                newTr += "<button class=\"btn btn-success btn-sm edit-btn\" value=\"" + response[i].id + "\">Edit</button>";
                newTr += "<button class=\"btn btn-danger btn-sm delete-btn\" value=\"" + response[i].id + "\">Delete</button>";
                newTr += "</td></tr>";

                $("#tbody").append(newTr);
            }
            
            $(".edit-btn").click(e => {
                let id = e.currentTarget.value;
                
                $("#editCard").val(id);
                
                $("#editCardModal").modal("show");
                
                request1 = $.ajax({url: "/eShop/json/profile/cards/" + id, type: "GET"});
                request1.done(response => {
                    $("#editCardFirstName").val(response.cardHolderFirstName);
                    $("#editCardLastName").val(response.cardHolderLastName);
                    $("#editCardNumber").val(response.cardNumber);
                    $("#editCardDate").val(response.cardExpiryDate);
                    $("#editCardCvv").val(response.cardCvv);
                });
            });
            
            $(".delete-btn").click(e => {
                let id = e.currentTarget.value;
                
                request2 = $.ajax({url: "/eShop/json/profile/cards/" + id, type: "DELETE"});
                request2.done(response => {
                    if (response.status === "failed") {
                        alert(response.message);
                    }
                    
                    if (response.status === "success") {
                        window.location.reload();
                    }
                });
            });
        }
    });
    
    $("#addCardButton").click(() => $("#addCardModal").modal("show"));
    
    $("#editCard").click(e => {
        let id = e.currentTarget.value;
        
        let firstName = $("#editCardFirstName").val();
        let lastName = $("#editCardLastName").val();
        let cardNumber = $("#editCardNumber").val();
        let cardDate = $("#editCardDate").val();
        let cvv = $("#editCardCvv").val();
        
        try {
            if (!firstName) throw "Enter first name!";
            if (!lastName) throw "Enter last name!";
            if (!cardNumber) throw "Enter card number!";
            if (!cardDate) throw "Enter card expiry date!";
            if (!cvv) throw "Enter card cvv!";
            if (firstName.match(/[\W_]+/)) throw "Enter valid first name!";
            if (lastName.match(/[\W_]+/)) throw "Enter valid last name!";
            if (!cardNumber.match(/^[\d ]+$/)) throw "Enter valid card number!";
            if (!cardDate.match(/^\d{2}[/]\d{2}$/)) throw "Enter valid card expiry date!";
            if (!cvv.match(/^\d{3}$/)) throw "Enter valid valid card cvv";
            
            $(".editCardError").text("");
            $(".editCardError").addClass("d-none");
            
            let data = {
                cardHolderFirstName: firstName,
                cardHolderLastName: lastName,
                cardNumber: cardNumber,
                cardExpiryDate: cardDate,
                cardCvv: cvv
            };
            
            request3 = $.ajax({
                url: "/eShop/json/profile/cards/" + id,
                type: "PUT",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });
            
            request3.done(response => {
                if (response.status === "failed") {
                    $(".editCardError").text(response.message);
                    $(".editCardError").removeClass("d-none");
                }
                
                if (response.status === "success") {
                    $(".editCardError").text("");
                    $(".editCardError").addClass("d-none");
                    
                    $("#editCardFirstName").val("");
                    $("#editCardLastName").val("");
                    $("#editCardNumber").val("");
                    $("#editCardDate").val("");
                    $("#editCardCvv").val("");
                    
                    $("#editCardModal").modal("hide");
                    
                    window.location.reload();
                }
            });
            
            request3.fail(errorThrown => {
                $(".editCardError").text(errorThrown);
                $(".editCardError").removeClass("d-none");
            });
        } catch (error) {
            $(".editCardError").text(error);
            $(".editCardError").removeClass("d-none");
        }
    });
});