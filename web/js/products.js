/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    let queryParams = new URLSearchParams(window.location.search);
    let catId = queryParams.has("cid") ? queryParams.get("cid") : "";

    request = $.ajax({url: "/eShop/json/products?cid=" + catId, type: "GET"});
    request.done(response => {
        $("#tbody").empty();

        for (let i = 0; i < response.length; i++) {
            let newTr = "<tr>";
            newTr += "<td>" + response[i].name + "</td>";
            newTr += "<td>" + response[i].description + "</td>";
            newTr += "<td>" + response[i].quantity + "</td>";
            newTr += "<td>" + response[i].price + "</td>";
            newTr += "<td><a href=\"products.html?cid=" + response[i].categoryId.id + "\">" + response[i].categoryId.name + "</a></td>";
            newTr += "<td class=\"d-flex\">";
            newTr += "<button class=\"btn btn-success btn-sm edit-btn\" value=\"" + response[i].id + "\">Edit</button>";
            newTr += "<button class=\"btn btn-danger btn-sm delete-btn\" value=\"" + response[i].id + "\">Delete</button>";
            newTr += "</td></tr>";

            $("#tbody").append(newTr);

            $(".delete-btn").unbind("click");
            $(".delete-btn").click(e => {
                let id = e.currentTarget.value;

                request6 = $.ajax({url: "/eShop/json/products/" + id, type: "DELETE"});
                request6.done(response => {
                    if (response.status === "failed") {
                        alert("Delete failed!");
                    }

                    if (response.status === "success") {
                        window.location.href = "products.html";
                    }
                });
            });

            $(".edit-btn").unbind("click");
            $(".edit-btn").click(e => {
                let id = e.currentTarget.value;

                $("#editProductModal").modal("show");

                request3 = $.ajax({url: "/eShop/json/categories", type: "GET"});
                request3.done(response => {
                    if (response[0].name) {
                        $("#editCategorySelect").empty();

                        for (let i = 0; i < response.length; i++) {
                            let newOption = "<option value=\"" + response[i].id + "\">" + response[i].name + "</option>";

                            $("#editCategorySelect").append(newOption);
                        }
                    }
                });

                request4 = $.ajax({url: "/eShop/json/products/" + id, type: "GET"});
                request4.done(response => {
                    if (response.name) {
                        $("#editCategorySelect").val(response.categoryId.id);
                        $("#editProductName").val(response.name);
                        $("#editDescription").val(response.description);
                        $("#editProductPrice").val(response.price);
                        $("#editProductImage").val(response.image);

                        $("#editProduct").val(response.id);
                    }
                });
            });
        }
    });

    $("#newProductButton").click(() => {
        $("#newProductModal").modal("show");

        request1 = $.ajax({url: "/eShop/json/categories", type: "GET"});
        request1.done(response => {
            $("#categorySelect").empty();

            for (let i = 0; i < response.length; i++) {
                let newOption = "<option value=\"" + response[i].id + "\">" + response[i].name + "</option>";

                $("#categorySelect").append(newOption);
            }
        });
    });

    $("#newProduct").click(() => {
        let categoryId = $("#categorySelect").val();
        let name = $("#productName").val();
        let description = $("#description").val();
        let price = $("#productPrice").val();
        let image = $("#productImage").val();

        try {
            if (!categoryId)
                throw "Select category!";
            if (!name)
                throw "Enter product name!";
            if (!description)
                throw "Enter description!";
            if (!price)
                throw "Enter price!";
            if (!image)
                throw "Enter product image!";

            $(".newProductError").text("");
            $(".newProductError").addClass("d-none");

            let data = {
                name: name,
                description: description,
                price: price,
                image: image
            };

            request2 = $.ajax({
                url: "/eShop/json/products?cid=" + categoryId,
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });

            request2.done(response => {
                if (response.status === "failed") {
                    $(".newProductError").text(response.message);
                    $(".newProductError").removeClass("d-none");
                }

                if (response.status === "success") {
                    $(".newProductError").text("");
                    $(".newProductError").addClass("d-none");

                    $("#productName").val("");
                    $("#description").val("");
                    $("#productPrice").val("");
                    $("#productImage").val("");

                    $("#newProductModal").modal("hide");

                    window.location.href = "products.html";
                }
            });

            request2.fail(errorThrown => {
                $(".newProductError").text(errorThrown);
                $(".newProductError").removeClass("d-none");
            });
        } catch (error) {
            $(".newProductError").text(error);
            $(".newProductError").removeClass("d-none");
        }
    });

    $("#editProduct").click(e => {
        let id = e.currentTarget.value;

        let categoryId = $("#editCategorySelect").val();
        let name = $("#editProductName").val();
        let description = $("#editDescription").val();
        let price = $("#editProductPrice").val();
        let image = $("#editProductImage").val();

        try {
            if (!categoryId)
                throw "Select category!";
            if (!name)
                throw "Enter product name!";
            if (!description)
                throw "Enter description!";
            if (!price)
                throw "Enter price!";
            if (!image) 
                throw "Enter product image!";

            $(".editProductError").text("");
            $(".editProductError").addClass("d-none");

            let data = {
                name: name,
                description: description,
                price: price,
                image: image
            };
            
            console.log(price);

            request5 = $.ajax({
                url: "/eShop/json/products/" + id,
                type: "PUT",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });

            request5.done(response => {
                if (response.status === "failed") {
                    $(".editProductError").text(response.message);
                    $(".editProductError").removeClass("d-none");
                }

                if (response.status === "success") {
                    $(".editProductError").text("");
                    $(".editProductError").addClass("d-none");

                    $("#editProductName").val("");
                    $("#editDescription").val("");
                    $("#editProductPrice").val("");
                    $("#editProductImage").val("");

                    $("#editProductModal").modal("hide");

                    window.location.href = "products.html";
                }
            });

            request5.fail(errorThrown => {
                $(".editProductError").text(errorThrown);
                $(".editProductError").removeClass("d-none");
            });
        } catch (error) {
            $(".editProductError").text(error);
            $(".editProductError").removeClass("d-none");
        }
    });

    $(".sort-btn").click(e => {
        let sort = e.currentTarget.value;
        let column = e.currentTarget.id;

        $("#" + column).val(sort === "esc" ? "desc" : "esc");

        request7 = $.ajax({url: "/eShop/json/products?cid=" + catId + "&sort=" + sort + "&col=" + column, type: "GET"});
        request7.done(response => {
            $("#tbody").empty();

            for (let i = 0; i < response.length; i++) {
                let newTr = "<tr>";
                newTr += "<td>" + response[i].name + "</td>";
                newTr += "<td>" + response[i].description + "</td>";
                newTr += "<td>" + response[i].quantity + "</td>";
                newTr += "<td>" + response[i].price + "</td>";
                newTr += "<td><a href=\"products.html?cid=" + response[i].categoryId.id + "\">" + response[i].categoryId.name + "</a></td>";
                newTr += "<td class=\"d-flex\">";
                newTr += "<button class=\"btn btn-success btn-sm edit-btn\" value=\"" + response[i].id + "\">Edit</button>";
                newTr += "<button class=\"btn btn-danger btn-sm delete-btn\" value=\"" + response[i].id + "\">Delete</button>";
                newTr += "</td></tr>";

                $("#tbody").append(newTr);

                $(".delete-btn").unbind("click");
                $(".delete-btn").click(e => {
                    let id = e.currentTarget.value;

                    request8 = $.ajax({url: "/eShop/json/products/" + id, type: "DELETE"});
                    request8.done(response => {
                        if (response.status === "failed") {
                            alert("Delete failed!");
                        }

                        if (response.status === "success") {
                            window.location.href = "products.html";
                        }
                    });
                });

                $(".edit-btn").unbind("click");
                $(".edit-btn").click(e => {
                    let id = e.currentTarget.value;

                    $("#editProductModal").modal("show");

                    request9 = $.ajax({url: "/eShop/json/categories", type: "GET"});
                    request9.done(response => {
                        if (response[0].name) {
                            $("#editCategorySelect").empty();

                            for (let i = 0; i < response.length; i++) {
                                let newOption = "<option value=\"" + response[i].id + "\">" + response[i].name + "</option>";

                                $("#editCategorySelect").append(newOption);
                            }
                        }
                    });

                    request10 = $.ajax({url: "/eShop/json/products/" + id, type: "GET"});
                    request10.done(response => {
                        if (response.name) {
                            $("#editCategorySelect").val(response.categoryId.id);
                            $("#editProductName").val(response.name);
                            $("#editDescription").val(response.description);
                            $("#editProductPrice").val(response.price);

                            $("#editProduct").val(response.id);
                        }
                    });
                });
            }
        });
    });
});