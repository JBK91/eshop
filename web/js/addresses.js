/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    request = $.ajax({url: "/eShop/json/profile/address", type: "GET"});
    request.done(response => {
        if (response !== undefined) {
            $("#tbody").empty();
            
            for (let i = 0; i < response.length; i++) {
                let newTr = "<tr>";
                newTr += "<td>" + response[i].address + "</td>";
                newTr += "<td>" + response[i].city + "</td>";
                newTr += "<td>" + response[i].country + "</td>";
                newTr += "<td>" + response[i].postCode + "</td>";
                newTr += "<td class=\"d-flex\">";
                newTr += "<button class=\"btn btn-success btn-sm edit-btn\" value=\"" + response[i].id + "\">Edit</button>";
                newTr += "<button class=\"btn btn-danger btn-sm delete-btn\" value=\"" + response[i].id + "\">Delete</button>";
                newTr += "</td></tr>";
                
                $("#tbody").append(newTr);
            }
            
            $(".edit-btn").click(e => {
                let id = e.currentTarget.value;
                
                $("#editAddressBtn").val(id);
                
                $("#editAddressModal").modal("show");
                
                request1 = $.ajax({url: "/eShop/json/profile/address/" + id, type: "GET"});
                request1.done(response => {
                    $("#editAddress").val(response.address);
                    $("#editCity").val(response.city);
                    $("#editCountry").val(response.country);
                    $("#editPostcode").val(response.postCode);
                });
            });
            
            $(".delete-btn").click(e => {
                let id = e.currentTarget.value;
                
                request3 = $.ajax({url: "/eShop/json/profile/address/" + id, type: "DELETE"});
                request3.done(response => {
                    if (response.status === "failed") {
                        alert(response.message);
                    }
                    
                    if (response.status === "success") {
                        window.location.reload();
                    }
                });
            });
        }
    });
    
    $("#addAddressButton").click(() => $("#addAddressModal").modal("show"));
    
    $("#editAddressBtn").click(e => {
        let id = e.currentTarget.value;
        
        let address = $("#editAddress").val();
        let city = $("#editCity").val();
        let country = $("#editCountry").val();
        let postcode = $("#editPostcode").val();
        
        try {
            if (!address) throw "Enter address!";
            if (!city) throw "Enter city!";
            if (!country) throw "Enter country!";
            if (!postcode) throw "Enter post code!";
            
            $(".editAddressError").text("");
            $(".editAddressError").addClass("d-none");
            
            let data = {
                address: address,
                city: city,
                country: country,
                postCode: postcode
            };
            
            request3 = $.ajax({
                url: "/eShop/json/profile/address/" + id,
                type: "PUT",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });
            
            request3.done(response => {
                if (response.status === "failed") {
                    $(".editAddressError").text(response.message);
                    $(".editAddressError").removeClass("d-none");
                }
                
                if (response.status === "success") {
                    $(".editAddressError").text("");
                    $(".editAddressError").addClass("d-none");
                    
                    $("#editAddress").val("");
                    $("#editCity").val("");
                    $("#editCountry").val("");
                    $("#editPostcode").val("");
                    
                    $("#editAddressModal").modal("hide");
                    
                    window.location.reload();
                }
            });
            
            request3.fail(errorThrown => {
                $(".editAddressError").text(errorThrown);
                $(".editAddressError").removeClass("d-none");
            });
        } catch (error) {
            $(".editAddressError").text(error);
            $(".editAddressError").removeClass("d-none");
        }
    });
});