/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    $("#header").empty();
    
    let nav = "<a href=\"/eShop/index.html\" class=\"btn btn-secondary\">Back</a>";
    nav += "<ul>";
    nav += "<li class=\"list-group-item\"><a href=\"categories.html\" class=\"btn btn-primary\">Categories</a>";
    nav += "<li class=\"list-group-item\"><a href=\"products.html\" class=\"btn btn-primary\">Products</a>";
    nav += "<li class=\"list-group-item\"><a href=\"receivings.html\" class=\"btn btn-primary\">Receivings</a>";
    nav += "<li class=\"list-group-item\"><a href=\"carts.html\" class=\"btn btn-primary\">Carts</a>";
    nav += "</ul>";
    
    $("#header").append(nav);
});