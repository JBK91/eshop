/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    request = $.ajax({url: "/eShop/json/profile/carts", type: "GET"});
    request.done(response => {
        if (response !== undefined) {
            $("#purchases").empty();
            
            for (let i = 0; i < response.length; i++) {
                let newTr = "<tr>";
                newTr += "<td>" + response[i].id + "</td>";
                newTr += "<td>" + response[i].datePurchased.split("T")[0] + "</td>";
                newTr += "<td>" + response[i].count + " <button class=\"btn btn-primary btn-sm show-product\" value=\"" + response[i].id + "\">Show</button></td>";
                newTr += "</tr>";
                
                $("#purchases").append(newTr);
            }
            
            $(".show-product").click(e => {
                let id = e.currentTarget.value;
                
                $("#showProductsModal").modal("show");
                
                request1 = $.ajax({url: "/eShop/json/profile/carts/data?cid=" + id, type: "GET"});
                request1.done(response => {
                    $("#products").empty();
                    
                    let totalQuantity = 0;
                    let totalPrice = 0;
                    
                    for (let i = 0; i < response.length; i++) {
                        let newTr = "<tr>";
                        newTr += "<td>" + response[i].productId.name + "</td>";
                        newTr += "<td>" + response[i].info + "</td>";
                        newTr += "<td>" + response[i].quantity + "</td>";
                        newTr += "<td>" + response[i].price + "</td>";
                        newTr += "</tr>";
                        
                        $("#products").append(newTr);
                        
                        totalQuantity += response[i].quantity;
                        totalPrice += response[i].price;
                    }
                    
                    let infoTr = "<tr><td>Total:</td><td></td><td>" + totalQuantity + "</td><td>" + totalPrice.toFixed(2) + "</td></tr>";
                    
                    $("#products").append(infoTr);
                });
            });
        }
    });
});