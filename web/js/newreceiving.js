/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    let productCount = 0;

    request = $.ajax({url: "/eShop/json/products", type: "GET"});
    request.done(response => {
        $("#productSelect").empty();

        for (let i = 0; i < response.length; i++) {
            let newOption = "<option value=\"" + response[i].id + "\">" + response[i].name + " (" + response[i].categoryId.name + ")</option>";

            $("#productSelect").append(newOption);
        }
    });

    $("#addProduct").click(() => {
        request1 = $.ajax({url: "/eShop/json/products", type: "GET"});
        request1.done(response => {
            let newProduct = "<div class=\"d-flex\">";
            newProduct += "<select id=\"productSelect" + productCount + "\"></select>";
            newProduct += "<input type=\"text\" class=\"form-control\" id=\"productQuantity" + productCount + "\" placeholder=\"Quantity\">";
            newProduct += "</div>";

            $("#products").append(newProduct);
            $("#productSelect" + productCount).empty();

            for (let i = 0; i < response.length; i++) {
                let newOption = "<option value=\"" + response[i].id + "\">" + response[i].name + " (" + response[i].categoryId.name + ")</option>";

                $("#productSelect" + productCount).append(newOption);
            }
        });

        productCount++;
    });
    
    $("#newReceiving").click(() => {
        let documentNumber = $("#documentNumber").val();
        let productId = $("#productSelect").val();
        let productQuantity = $("#productQuantity").val();
        
        let products = [];
        
        products.push({productId: productId, quantity: productQuantity});
        
        try {
            if (!documentNumber) throw "Enter receiving document number!";
            if (!productId) throw "Select product!";
            if (!productQuantity) throw "Enter product quantity!";
            
            for (let i = 1; i <= productCount; i++) {
                let otherProductId = $("#productSelect" + i).val();
                let quantity = $("#productQuantity" + i).val();
                
                if (!otherProductId) throw "Select product!";
                if (!quantity) throw "Enter product quantity!";
                
                let added = false;
                
                for (let j = 0; j < products.length; j++) {
                    if (products[j].productId === otherProductId) {
                        products[j].quantity = parseInt(products[j].quantity) + parseInt(quantity);
                        added = true;
                        break;
                    }
                }
                
                if (!added) {
                    products.push({productId: otherProductId, quantity: quantity});
                }
            }
            
            $(".receivingError").text("");
            $(".receivingError").addClass("d-none");
            
            let data = {
                documentNumber: documentNumber,
                products: products
            };
            
            request2 = $.ajax({
                url: "/eShop/json/receivings/data",
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });
            
            request2.done(response => {
                if (response.status === "failed") {
                    $(".receivingError").text(response.message);
                    $(".receivingError").removeClass("d-none");
                }
                
                if (response.status === "success") {
                    window.location.href = "receivings.html";
                }
            });
            
            request2.fail(errorThrown => {
                $(".receivingError").text(errorThrown);
                $(".receivingError").removeClass("d-none");
            });
        } catch (error) {
            $(".receivingError").text(error);
            $(".receivingError").removeClass("d-none");
        }
    });
});