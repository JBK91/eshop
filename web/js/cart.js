/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    request = $.ajax({url: "json/carts/data", type: "GET"});
    request.done(response => {
        $("#items").empty();
        
        let totalPrice = 0;
        let totalQuantity = 0;
        
        if (response.length > 0) {
            $("#checkoutButton").removeClass("d-none");
        }
        
        if (response !== undefined) {
            for (let i = 0; i < response.length; i++) {
                let newTr = "<tr>";
                newTr += "<td>" + response[i].productId.name + "</td>";
                newTr += "<td>Vegetable glycerin: " + response[i].productId.description.split("/")[0];
                newTr += "<br>Propylene glycol: " + response[i].productId.description.split(", ")[0].split("/")[1];
                newTr += "<br>Capacity: " + response[i].productId.description.split(", ")[1];
                newTr += "<br>Strength: " + response[i].info;
                newTr += "</td><td><select class=\"change-quantity\" id=\"quantity" + response[i].id + "\">";

                for (let j = 1; j <= response[i].productId.quantity; j++) {
                    newTr += "<option value=\"" + j + "\">" + j + "</option>";
                }

                newTr += "</select></td><td>" + response[i].price  + "€" + "</td>";
                newTr += "<td><button class=\"btn btn-danger delete-btn\" value=\"" + response[i].id + "\">Delete</button></td></tr>";

                $("#items").append(newTr);

                $("#quantity" + response[i].id).val(response[i].quantity);

                totalPrice += response[i].price;
                totalQuantity += response[i].quantity;
            }

            let infoTr = "<tr><td>Total: </td><td>" + response.length + " Products </td><td>" + totalQuantity + "</td><td>" + totalPrice.toFixed(2)  + "€</td></tr>";

            $("#items").append(infoTr);
            
            $(".delete-btn").unbind("click");
            $(".delete-btn").click(e => {
                let id = e.currentTarget.value;

                request2 = $.ajax({url: "json/carts/data/" + id, type: "DELETE"});
                request2.done(response2 => {
                    if (response2.status === "failed") {
                        alert(response2.message);
                    }

                    if (response2.status === "success") {
                        window.location.reload();
                    }
                });
            });
            
            $(".change-quantity").unbind("change");
            $(".change-quantity").change(e => {
                let cartDataId = e.currentTarget.id.split("quantity")[1];

                let quantity = $("#quantity" + cartDataId).val();

                let data = {
                    quantity: quantity
                };

                request1 = $.ajax({
                    url: "json/carts/data/" + cartDataId,
                    type: "PUT",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                });

                request1.done(response1 => {
                    if (response1.status === "failed") {
                        alert(response1.message);
                    }

                    if (response1.status === "success") {
                        window.location.reload();
                    }
                });
            });
        }
    });
});