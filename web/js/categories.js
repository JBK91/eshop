/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    request = $.ajax({url: "/eShop/json/categories"});
    request.done(response => {
        $("#tbody").empty();

        for (let i = 0; i < response.length; i++) {
            let newTr = "<tr>";
            newTr += "<td><a href=\"products.html?cid=" + response[i].id + "\">" + response[i].name + "</a></td>";
            newTr += "<td>" + response[i].count + "</td>";
            newTr += "<td class=\"d-flex\">";
            newTr += "<button class=\"btn btn-success btn-sm edit-btn\" value=\"" + response[i].id + "\">Edit</button>";
            newTr += "<button class=\"btn btn-danger btn-sm delete-btn\" value=\"" + response[i].id + "\">Delete</button>";
            newTr += "</td></tr>";

            $("#tbody").append(newTr);

            $(".delete-btn").unbind("click");

            $(".delete-btn").click(e => {
                let id = e.currentTarget.value;

                request3 = $.ajax({url: "/eShop/json/categories/" + id, type: "DELETE"});
                request3.done(response => {
                    if (response.status === "failed") {
                        alert("Delete failed!");
                    }

                    if (response.status === "success") {
                        window.location.href = "categories.html";
                    }
                });
            });

            $(".edit-btn").unbind("click");

            $(".edit-btn").click(e => {
                let id = e.currentTarget.value;

                $("#editCategoryModal").modal("show");

                $("#editCategorybtn").val(id);

                request2 = $.ajax({url: "/eShop/json/categories/" + id, type: "GET"});
                request2.done(response => {
                    if (response.name) {
                        $("#editCategoryName").val(response.name);
                    } else {
                        window.location.reload();
                    }
                });
            });
        }
    });

    $("#newCategoryButton").click(() => $("#newCategoryModal").modal("show"));

    $("#newCategorybtn").click(() => {
        let name = $("#categoryName").val();

        try {
            if (!name)
                throw "Enter category name!";

            $(".newCategoryError").text("");
            $(".newCategoryError").addClass("d-none");

            let data = {
                name: name
            };

            request1 = $.ajax({
                url: "/eShop/json/categories",
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });

            request1.done(response => {
                if (response.status === "failed") {
                    $(".newCategoryError").text(response.message);
                    $(".newCategoryError").removeClass("d-none");
                }

                if (response.status === "success") {
                    $(".newCategoryError").text("");
                    $(".newCategoryError").addClass("d-none");

                    $("#categoryName").val("");

                    $("#newCategoryModal").modal("hide");

                    window.location.href = "categories.html";
                }
            });

            request1.fail(errorThrown => {
                $(".newCategoryError").text(errorThrown);
                $(".newCategoryError").removeClass("d-none");
            });
        } catch (error) {
            $(".newCategoryError").text(error);
            $(".newCategoryError").removeClass("d-none");
        }
    });

    $("#editCategorybtn").click(e => {
        let id = e.currentTarget.value;

        let name = $("#editCategoryName").val();

        try {
            if (!name)
                throw "Enter category name!";

            $(".editCategoryError").text("");
            $(".editCategoryError").addClass("d-none");

            let data = {
                name: name
            };

            request1 = $.ajax({
                url: "/eShop/json/categories/" + id,
                type: "PUT",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });

            request1.done(response => {
                if (response.status === "failed") {
                    $(".editCategoryError").text(response.message);
                    $(".editCategoryError").removeClass("d-none");
                }

                if (response.status === "success") {
                    $(".editCategoryError").text("");
                    $(".editCategoryError").addClass("d-none");

                    $("#editCategoryName").val("");

                    $("#editCategoryModal").modal("hide");

                    window.location.href = "categories.html";
                }
            });

            request1.fail(errorThrown => {
                $(".editCategoryError").text(errorThrown);
                $(".editCategoryError").removeClass("d-none");
            });
        } catch (error) {
            $(".editCategoryError").text(error);
            $(".editCategoryError").removeClass("d-none");
        }
    });

    $(".sort-btn").click(e => {
        let sort = e.currentTarget.value;
        let order = e.currentTarget.id;

        $("#" + order).val(sort === "desc" ? "esc" : "desc");

        request4 = $.ajax({url: "/eShop/json/categories?sort=" + sort + "&order=" + order, type: "GET"});
        request4.done(response => {
            $("#tbody").empty();

            for (let i = 0; i < response.length; i++) {
                let newTr = "<tr>";
                newTr += "<td><a href=\"products.html?cid=" + response[i].id + "\">" + response[i].name + "</a></td>";
                newTr += "<td>" + response[i].count + "</td>";
                newTr += "<td class=\"d-flex\">";
                newTr += "<button class=\"btn btn-success btn-sm edit-btn\" value=\"" + response[i].id + "\">Edit</button>";
                newTr += "<button class=\"btn btn-danger btn-sm delete-btn\" value=\"" + response[i].id + "\">Delete</button>";
                newTr += "</td></tr>";

                $("#tbody").append(newTr);

                $(".delete-btn").unbind("click");

                $(".delete-btn").click(e => {
                    let id = e.currentTarget.value;

                    request3 = $.ajax({url: "/eShop/json/categories/" + id, type: "DELETE"});
                    request3.done(response => {
                        if (response.status === "failed") {
                            alert("Delete failed!");
                        }

                        if (response.status === "success") {
                            window.location.reload();
                        }
                    });
                });

                $(".edit-btn").unbind("click");

                $(".edit-btn").click(e => {
                    let id = e.currentTarget.value;

                    $("#editCategoryModal").modal("show");

                    $("#editCategorybtn").val(id);

                    request2 = $.ajax({url: "/eShop/json/categories/" + id, type: "GET"});
                    request2.done(response => {
                        if (response.name) {
                            $("#categoryName").val(response.name);
                        } else {
                            window.location.reload();
                        }
                    });
                });
            }
        });
    });
});