/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    let queryParams = new URLSearchParams(window.location.search);
    
    if (!queryParams.has("product")) {
        window.location.href = "index.html";
    }
    
    let productId = queryParams.get("product");
    
    if (isNaN(productId)) {
        window.location.href = "index.html";
    }
    
    request = $.ajax({url: "json/products/" + productId, type: "GET"});
    request.done(response => {
        if (response === undefined) {
            window.location.href = "index.html";
        } else {
            $("#image").empty();
            $("#image").append("<img src=\"img/" + response.image + "\" alt=\"" + response.name + "\">");
            $("#productName").text(response.name);
            $("#vg").text(response.description.split("/")[0]);
            $("#pg").text(response.description.split(", ")[0].split("/")[1]);
            $("#capacity").text(response.description.split(", ")[1]);
            $("#quantity").text(response.quantity);
            $("#price").text(response.price + "€");
            
            $("#quantitytobuy").empty();
            
            for (let i = 1; i <= response.quantity; i++) {
                let newOption = "<option value=\"" + i +"\">" + i + "</option>";
                
                $("#quantitytobuy").append(newOption);
            }
        }
    });
    
    $("#toCart").click(() => {
        let info = $("#info").val();
        let quantity = $("#quantitytobuy").val();
        
        let data = {
            quantity: quantity,
            info: info
        };
        
        request1 = $.ajax({
            url: "json/carts?pid=" + productId,
            type: "POST",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        });
        
        request1.done(response => {
            if (response.status === "failed") {
                alert(response.message);
            }
            
            if (response.status === "success") {
                request2 = $.ajax({url: "json/carts/count", type: "GET"});
                request2.done(response1 => $("#cartSize").text(response1.status));
                window.location.reload();
            }
        });
    });
});