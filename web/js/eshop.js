/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
async function getProducts(url) {
    return new Promise(resolve => {
        request6 = $.ajax({url: url, type: "GET"});
        request6.done(response => {
            resolve(response);
        });
    });
}

async function printProducts(products) {
    let pages = Math.ceil(await products.length / 20);

    let currentPage = 1;

    $("#pages").empty();

    for (let i = 1; i <= pages; i++) {
        let newPage = "<button class=\"btn btn-secondary btn-sm page-btn \" id=\"page" + i + "\" value=\"" + i + "\">" + i + "</button>";

        $("#pages").append(newPage);
    }

    $("#page" + currentPage).addClass("disabled");

    $("#products").empty();

    let end = products.length > 20 ? 20 : products.length;

    for (let i = 0; i < end; i++) {
        let stock;

        if (products[i].quantity === 0) {
            stock = "<p class=\"text-danger\">Out of Stock!</p>";
        } else {
            stock = "<p class=\"text-success\">In Stock</p>";
        }

        let newProduct = "<div><div class=\"product\" id=\"product" + products[i].id + "\">";
        newProduct += "<img src=\"img/" + products[i].image + "\" alt=\"" + products[i].name + "\">";
        newProduct += "</div>";
        newProduct += "<a href=\"product.html?product=" + products[i].id + "\">" + products[i].name + "</a>";
        newProduct += "<p>" + products[i].price + "€</p>";
        newProduct += stock;
        newProduct += "</div>";

        $("#products").append(newProduct);
    }

    $(".page-btn").click(e => {
        $("#page" + currentPage).removeClass("disabled");
        currentPage = e.currentTarget.value;

        $("#page" + currentPage).addClass("disabled");

        $("#products").empty();

        let pageContent = (currentPage - 1) * 20;
        let lastPage = pageContent + 20 > products.length ? products.length : pageContent + 20;

        for (let i = pageContent; i < lastPage; i++) {
            if (products[i].quantity === 0) {
                stock = "<p class=\"text-danger\">Out of Stock!</p>";
            } else {
                stock = "<p class=\"text-success\">In Stock</p>";
            }

            let newProduct = "<div><div class=\"product\" id=\"product" + products[i].id + "\">";
            newProduct += "<img src=\"img/" + products[i].image + "\" alt=\"" + products[i].name + "\">";
            newProduct += "</div>";
            newProduct += "<a href=\"product.html?product=" + products[i].id + "\">" + products[i].name + "</a>";
            newProduct += "<p>" + products[i].price + "€</p>";
            newProduct += stock;
            newProduct += "</div>";

            $("#products").append(newProduct);

            window.scrollTo(0, 0);
        }
    });
}

$(document).ready(async () => {
    let products = await getProducts("json/products");

    printProducts(products);

    request5 = $.ajax({url: "json/categories"});
    request5.done(response => {
        $("#categories").empty();

        for (let i = 0; i < response.length; i++) {
            let newCheckInput = "<div><input type=\"checkbox\" class=\"check-box\" name=\"category\" id=\"category" + i + "\" value=\"" + response[i].id + "\">";
            newCheckInput += "<label for=\"category" + i + "\">" + response[i].name + "</label></div>";

            $("#categories").append(newCheckInput);
        }

        $(".check-box").unbind("change");

        $(".check-box").change(async () => {
            let childs = $("#categories").children().length;

            let categories = [];

            for (let i = 0; i < childs; i++) {
                if ($("#category" + i).is(":checked")) {
                    categories.push($("#category" + i).val());
                }
            }

            let url;

            if (categories.length > 1) {
                url = "json/products/cats?cats=" + categories.join(":");
            } else if (categories.length === 0) {
                url = "json/products";
            } else {
                url = "json/products?cid=" + categories[0];
            }

            products = await getProducts(url);

            printProducts(products);
        });
    });
});