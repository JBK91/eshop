/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    request = $.ajax({url: "/eShop/json/receivings", type: "GET"});
    request.done(response => {
        $("#tbody").empty();

        for (let i = 0; i < response.length; i++) {
            let newTr = "<tr>";
            newTr += "<td><a href=\"receiving.html?rid=" + response[i].id + "\">" + response[i].documentNumber + "</a></td>";
            newTr += "<td>" + response[i].receivingDate.split("T")[0] + "</td>";
            newTr += "<td>" + response[i].count + "</td>";
            newTr += "</tr>";
            
            $("#tbody").append(newTr);
        }
    });

    $(".sort-btn").click(e => {
        let sort = e.currentTarget.value;
        let column = e.currentTarget.id;

        $("#" + column).val(sort === "esc" ? "desc" : "esc");

        request1 = $.ajax({url: "/eShop/json/receivings?sort=" + sort + "&col=" + column, type: "GET"});
        request1.done(response => {
            $("#tbody").empty();

            for (let i = 0; i < response.length; i++) {
                let newTr = "<tr>";
                newTr += "<td><a href=\"receiving.html?rid=" + response[i].id + "\">" + response[i].documentNumber + "</a></td>";
                newTr += "<td>" + response[i].receivingDate.split("T")[0] + "</td>";
                newTr += "<td>" + response[i].count + "</td>";
                newTr += "</tr>";
                
                $("#tbody").append(newTr);
            }
        });
    });
});