/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function printCarts(url) {
    request = $.ajax({url: url, type: "GET"});
    request.done(response => {
        $("#tbody").empty();

        for (let i = 0; i < response.length; i++) {
            let datePurchased = response[i].datePurchased === undefined ? "-" : response[i].datePurchased.split("T")[0];

            let newTr = "<tr>";
            newTr += "<td>" + response[i].id + "</td>";
            newTr += "<td>" + response[i].dateCreated.split("T")[0] + "</td>";
            newTr += "<td>" + datePurchased + "</td>";
            newTr += "<td>" + response[i].userId.username + "</td>";
            newTr += "<td>" + response[i].count + " <button class=\"btn btn-primary btn-sm show-products\" value=\"" + response[i].id + "\">Show</button></td></tr>";

            $("#tbody").append(newTr);
        }

        $(".show-products").click(e => {
            let id = e.currentTarget.value;

            $("#productsModal").modal("show");

            request1 = $.ajax({url: "/eShop/json/carts/adata?cid=" + id, type: "GET"});
            request1.done(response1 => {
                $("#exampleModalCenterTitle").text(response1[0].cartId.userId.username + " Cart");

                $("#productsTbody").empty();

                let totalPrice = 0;
                let totalQuantity = 0;

                for (let i = 0; i < response1.length; i++) {
                    let newTr = "<tr>";
                    newTr += "<td>" + response1[i].productId.name + "</td>";
                    newTr += "<td>" + response1[i].info + "</td>";
                    newTr += "<td>" + response1[i].quantity + "</td>";
                    newTr += "<td>" + response1[i].price + "</td></tr>";

                    $("#productsTbody").append(newTr);

                    totalPrice += response1[i].price;
                    totalQuantity += response1[i].quantity;
                }

                let infoTr = "<tr><td>Total:</td><td></td><td>" + totalQuantity + "</td><td>" + totalPrice.toFixed(2) + "</td></tr>";

                $("#productsTbody").append(infoTr);
            });
        });
    });
}

$(document).ready(() => {
    printCarts("/eShop/json/carts");
    
    $("#filter").change(() => {
        let filter = $("#filter").val();
        
        let url = "/eShop/json/carts";
        
        if (filter === "bought") {
            url = "/eShop/json/carts?b=true";
        }
        
        if (filter === "notbought") {
            url = "/eShop/json/carts?b=false";
        }
        
        printCarts(url);
    });
    
    $(".sort-btn").click(e => {
        let sort = e.currentTarget.value;
        let column = e.currentTarget.id;
        let filter = $("#filter").val();
        
        $("#" + column).val(sort === "esc" ? "desc" : "esc");
        
        if (filter !== "all") {
            printCarts("/eShop/json/carts?b=" + filter === "bought" ? "true" : "false" + "&sort=" + sort + "&col=" + column);
        } else {
            printCarts("/eShop/json/carts?sort=" + sort + "&col=" + column);
        }
    });
});