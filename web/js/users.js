/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(() => {
    request5 = $.ajax({url: "/eShop/json/carts/data", type: "GET"});
    request5.done(response => {
        if (response === undefined) {
            $("#cartSize").text(0);
        } else {
            $("#cartSize").text(response.length);
        }
    });
    
    request = $.ajax({url: "/eShop/json/users/logged", type: "GET"});
    request.done(response => {
        if (response.status === "logged") {
            $("#nav").removeClass("d-flex");
            $("#nav").addClass("d-none");
            $("#userNav").removeClass("d-none");
            $("#userNav").addClass("d-flex");

            $("#profileButton").text(response.message + " profile");
        } else {
            $("#nav").removeClass("d-none");
            $("#nav").addClass("d-flex");
            $("#userNav").removeClass("d-flex");
            $("#userNav").addClass("d-none");
        }
    });

    request1 = $.ajax({url: "/eShop/json/users/isadmin", type: "GET"});
    request1.done(response => {
        if (response.status === "admin") {
            $("#adminButton").removeClass("d-none");
        } else {
            $("#adminButton").addClass("d-none");
        }
    });

    $("#registerButton").click(() => $("#registerModal").modal("show"));

    $("#registerbtn").click(() => {
        let username = $("#username").val();
        let email = $("#email").val();
        let password1 = $("#password1").val();
        let password2 = $("#password2").val();

        try {
            if (!username)
                throw "Enter a username!";
            if (!email)
                throw "Enter a email!";
            if (!password1)
                throw "Enter password!";
            if (!password2)
                throw "Repeat password!";
            if (username.match(/[\W_]+/))
                throw "Username contains illegal characters!";
            if (password1.match(/[\W_]+/))
                throw "Password contains illegal characters!";
            if (password1.length < 5)
                throw "Password must be longer than 5 characters!";
            if (username.length < 3)
                throw "Username must be longer than 3 characters!";
            if (!email.match(/\w+@\w+[.]\w+/))
                throw "Enter a valid email";
            if (password1 !== password2)
                throw "Password don't matches!";

            $(".registerError").text("");
            $(".registerError").addClass("d-none");

            let data = {
                username: username,
                password: password1,
                email: email
            };

            request2 = $.ajax({
                url: "/eShop/json/users/register",
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });

            request2.done(response => {
                if (response.status === "failed") {
                    $(".registerError").text(response.message);
                    $(".registerError").removeClass("d-none");
                }

                if (response.status === "success") {
                    $(".registerError").text("");
                    $(".registerError").addClass("d-none");

                    $("#username").val("");
                    $("#email").val("");
                    $("#password1").val("");
                    $("#password2").val("");

                    $("#registerModal").modal("hide");

                    window.location.reload();
                }
            });

            request2.fail(errorThrown => {
                $(".registerError").text(errorThrown);
                $(".registerError").removeClass("d-none");
            });
        } catch (error) {
            $(".registerError").text(error);
            $(".registerError").removeClass("d-none");
        }
    });

    $("#loginButton").click(() => $("#loginModal").modal("show"));

    $("#loginbtn").click(() => {
        let username = $("#user").val();
        let password = $("#password").val();

        try {
            if (!username)
                throw "Enter a username!";
            if (!password)
                throw "Enter password!";
            if (username.match(/[\W_]+/))
                throw "Username contains illegal characters!";
            if (password.match(/[\W_]+/))
                throw "Password contains illegal characters!";

            $(".loginError").text("");
            $(".loginError").addClass("d-none");

            let data = {
                username: username,
                password: password
            };

            request3 = $.ajax({
                url: "/eShop/json/users/login",
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });

            request3.done(response => {
                if (response.status === "failed") {
                    $(".loginError").text(response.message);
                    $(".loginError").removeClass("d-none");
                }

                if (response.status === "success") {
                    $(".loginError").text("");
                    $(".loginError").addClass("d-none");

                    $("#user").val("");
                    $("#password").val("");

                    $("#loginModal").modal("hide");

                    window.location.reload();
                }
            });

            request3.fail(errorThrown => {
                $(".loginError").text(errorThrown);
                $(".loginError").removeClass("d-none");
            });
        } catch (error) {
            $(".loginError").text(error);
            $(".loginError").removeClass("d-none");
        }
    });

    $("#logoutButton").click(() => {
        request4 = $.ajax({url: "/eShop/json/users/logout", type: "GET"});
        request4.done(() => window.location.reload());
    });
});